import json
import random
from pathlib import Path
from threading import Lock
from typing import Any, ClassVar, Optional

import pandas as pd


class Singleton(type):
    _instances: ClassVar[dict[type["Singleton"], "Singleton"]] = {}
    _lock: Lock = Lock()

    def __call__(cls, *args, **kwargs):
        with cls._lock:
            if cls not in cls._instances:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
        return cls._instances[cls]


class TabularData:
    _data: dict[str, pd.DataFrame] = {}
    _data_dir: Path

    def __init__(self, data_dir: Path = Path()) -> None:
        if not data_dir.exists():
            raise FileNotFoundError(f"Directory not found: '{data_dir}'")
        if not data_dir.is_dir():
            raise NotADirectoryError(f"Not a directory: '{data_dir}'")
        self._data_dir = data_dir

    def load_all(self) -> None:
        for file in self._data_dir.glob("*.csv"):
            self._data[file.stem] = pd.read_csv(file)

    def load(self, file_stem: str) -> None:
        file = self._data_dir / f"{file_stem}.csv"
        if not file.exists():
            raise FileNotFoundError(f"File not found: '{file}'")
        self._data[file.stem] = pd.read_csv(file)

    def get_data_frame(self, file_stem: str) -> pd.DataFrame:
        if file_stem not in self._data:
            self.load(file_stem)
        return self._data[file_stem]

    def get_row(self, file_stem: str, column: str, value) -> list[Any]:
        df = self.get_data_frame(file_stem)
        records = df.loc[df[column] == value].values.tolist()
        return records.pop() if records else []

    def get_random_row(self, file_stem: str) -> list[Any]:
        df = self.get_data_frame(file_stem)
        return df.sample().to_list()[0] if not df.empty else []  # type: ignore[operator]
        # issue of pandas-stubs==2.2.3

    def get_record(self, file_stem: str, column: str, value) -> dict[str, Any]:
        df = self.get_data_frame(file_stem)
        records = df.loc[df[column] == value].to_dict("records")
        return records.pop() if records else {}

    def get_random_record(self, file_stem: str) -> dict:
        df = self.get_data_frame(file_stem)
        return df.sample().to_dict("records")[0] if not df.empty else {}  # type: ignore


class TreeData:
    _data: dict[str, dict] = {}
    _data_dir: Path

    def __init__(self, data_dir: Path = Path()) -> None:
        if not data_dir.exists():
            raise FileNotFoundError(f"Directory not found: '{data_dir}'")
        if not data_dir.is_dir():
            raise NotADirectoryError(f"Not a directory: '{data_dir}'")
        self._data_dir = data_dir

    def load_all(self) -> None:
        for filepath in self._data_dir.glob("*.json"):
            file = open(filepath, "r")
            self._data.update(json.load(file))
            file.close()

    def load(self, file_stem: str) -> None:
        filepath = self._data_dir / f"{file_stem}.json"
        if not filepath.exists():
            raise FileNotFoundError(f"File not found: '{filepath}'")
        file = open(filepath, "r")
        self._data.update(json.load(file))
        file.close()

    def get_random_value(self, class_iri: str, property_iri: str) -> str:
        if (
            class_iri not in self._data
            or property_iri not in self._data[class_iri]
            or not self._data[class_iri][property_iri]
        ):
            return ""
        return random.choice(self._data[class_iri][property_iri])


class RegistryData:
    _data: dict[str, Any] = {}

    def __init__(self, data: Optional[dict[str, Any]] = None) -> None:
        if data:
            self._data = data

    def get_item(self, key: str) -> Any:
        return self._data.get(key, None)

    def set_item(self, key: str, value: Any) -> None:
        self._data[key] = value


class TabularDataStore(TabularData, metaclass=Singleton):
    pass


class TreeDataStore(TreeData, metaclass=Singleton):
    pass


class Registry(RegistryData, metaclass=Singleton):
    pass

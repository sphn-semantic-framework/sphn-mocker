import hashlib
import logging
import random
import uuid
from typing import Any, Optional
from datetime import datetime, timezone

from faker import Faker
from faker.providers import BaseProvider

from .data_stores import Registry, TabularDataStore, TreeDataStore

# Instantiate the logger
logger = logging.getLogger(__name__)

CODING_SYSTEM_IRIS = {
    "https://www.whocc.no/atc_ddd_index/?code=": "ATC",
    "https://biomedit.ch/rdf/sphn-resource/chop/": "CHOP",
    "http://purl.obolibrary.org/obo/ECO_": "ECO",
    "http://edamontology.org/": "EDAM",
    "http://www.ebi.ac.uk/efo/EFO_": "EFO",
    "https://biomedit.ch/rdf/sphn-resource/emdn/": "EMDN",
    "http://purl.obolibrary.org/obo/GENEPIO_": "GENEPIO",
    "http://purl.obolibrary.org/obo/GENO_": "GENO",
    "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/": "HGNC",
    "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/": "ICD-10-GM",
    "https://loinc.org/rdf/": "LOINC",
    "http://purl.obolibrary.org/obo/OBI_": "OBI",
    "http://www.orpha.net/ORDO/": "ORDO",
    "http://snomed.info/id/": "SNOMED-CT",
    "http://purl.obolibrary.org/obo/SO_": "SO",
    "https://biomedit.ch/rdf/sphn-resource/ucum/": "UCUM",
}

FALLBACK_RELATIONSHIPS = {
    "sphn:hasUnit/sphn:hasCode": ["sphn:Unit", "sphn:hasCode"],
    "sphn:hasBodySite/sphn:hasCode": ["sphn:BodySite", "sphn:hasCode"],
    "sphn:hasIntent/sphn:hasCode": ["sphn:Intent", "sphn:hasCode"],
}


class CustomData:
    concept_ids: dict[str, Any] = {}

    def __init__(self, faker: Faker):
        self.faker = faker

    def _get_id(self) -> str:
        random.seed(str(uuid.uuid4()))
        return str(uuid.UUID(int=random.getrandbits(128), version=4))

    def _get_termid(self, iri: str) -> str:
        def format(termid: str) -> str:
            replace = [" ", "/"]
            replace_with = "-"
            for r in replace:
                termid = termid.replace(r, replace_with)
            return termid

        for base_iri, coding_system in CODING_SYSTEM_IRIS.items():
            if iri.startswith(base_iri):
                unique_id = iri[len(base_iri) :]
                return format(f"{coding_system}-{unique_id}")
        unique_id = hashlib.shake_128(str.encode(iri)).hexdigest(4)
        return format("UNKNOWN-" + unique_id)

    def sample(self, class_iri: str, property_iri: str) -> str:
        dataset = TreeDataStore()
        value = dataset.get_random_value(class_iri, property_iri)
        if not value:
            for path, relationship in FALLBACK_RELATIONSHIPS.items():
                if property_iri.endswith(path):
                    return self.sample(relationship[0], relationship[1])
            faker = Faker()
            return faker.bothify("https://concept.example/id/########")
        return value

    def terminology(self, class_iri: str, property_iri: str) -> dict[str, Any]:
        iri = self.sample(class_iri, property_iri)
        return {
            "termid": self._get_termid(iri),
            "iri": iri,
        }

    def id(self, concept_iri: Optional[str] = None, is_core_concept: bool = False) -> str:
        registry = Registry()
        core_concepts = registry.get_item("core_concepts") or []
        if not concept_iri or concept_iri not in core_concepts:
            return self._get_id()
        if concept_iri not in CustomData.concept_ids:
            CustomData.concept_ids[concept_iri] = {}
            CustomData.concept_ids[concept_iri][self._get_id()] = False
        if is_core_concept:
            for idx, used in CustomData.concept_ids[concept_iri].items():
                if not used:
                    CustomData.concept_ids[concept_iri][idx] = True
                    return idx
        concept_ids = list(CustomData.concept_ids[concept_iri].keys())
        return random.sample(concept_ids, 1)[0]

    def timestamp(self, start_date: str, end_date: str) -> str:
        faker = Faker()
        return faker.date_time_between(
            datetime.fromisoformat(start_date), datetime.fromisoformat(end_date), timezone.utc
        ).isoformat()

    def code(self, id: str = "") -> dict[str, Any]:
        dataset = TabularDataStore()
        file_stem = "code"
        properties = [
            "id",
            "sphn:hasIdentifier",
            "sphn:hasName",
            "sphn:hasCodingSystemAndVersion",
        ]
        row: list[Any]
        if id:
            row = dataset.get_row(file_stem, "code", id)
        else:
            row = dataset.get_random_row(file_stem)
        return dict(zip(properties, row))

    def institute(self) -> dict[str, Any]:
        dataset = TabularDataStore()
        id, code = dataset.get_random_row("data_provider_institute")
        return {
            "id": id,
            "sphn:hasCode": self.code(code),
        }

    def subject_pseudo_identifier(self, code: str = "") -> dict[str, Any]:
        faker = Faker()
        if not code:
            code = faker.bothify("CHE-###_###_###")
        id = faker.bothify("Patient-?###", "ABC")
        return {
            "id": f"{code}-SubjectPseudoIdentifier-{id}",
            "sphn:hasIdentifier": id,
        }


class CustomDataProvider(BaseProvider, CustomData):
    pass

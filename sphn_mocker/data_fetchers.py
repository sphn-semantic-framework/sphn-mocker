from pathlib import Path

import pandas as pd
from . import LATEST_DATA_TAG


def get_data_path(search_path: str, tag: str) -> Path:
    if tag == "latest":
        tag = LATEST_DATA_TAG

    package_name = __name__.split(".")[0]
    file_base_dir = Path(package_name) / f"files/{tag}"
    data_objs = list(file_base_dir.glob(search_path))

    if (n_data_objs := len(data_objs)) != 1:
        raise RuntimeError(f"Expected 1 file - found {n_data_objs} for {file_base_dir / search_path}:\n{data_objs}")

    data_obj = data_objs[0]
    return data_obj


def get_schema_file(tag: str) -> Path:
    return get_data_path("schema/*.ttl", tag)


def get_value_sets_file(tag: str) -> Path:
    return get_data_path("samples/value_sets/*.json", tag)


def get_fixtures_dir(tag: str) -> Path:
    return get_data_path("samples/fixtures", tag)


def get_value_sets_dir(tag: str) -> Path:
    return get_data_path("samples/value_sets", tag)


def get_sampler_checks_file(tag: str) -> Path:
    return get_data_path("samples/sampler_checks/*.json", tag)


def get_data_tags() -> list[str]:
    package_name = __name__.split(".")[0]
    file_dir = Path(package_name) / "files"
    tags = [str(p.name) for p in file_dir.glob("*/")]
    tags = ["latest"] + tags
    return tags


def get_random_data_provider(tag: str) -> str:
    data_provider_file = get_fixtures_dir(tag) / "data_provider.csv"
    df = pd.read_csv(data_provider_file)
    dp: str = df.DataProvider.sample().values[0]
    dp = dp.replace("-DataProvider", "")
    return dp

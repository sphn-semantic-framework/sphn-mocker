import json
import logging
import re
from copy import deepcopy
from pathlib import Path
from typing import Any, Callable, Optional, Union

from faker import Faker
from jsf import JSF

from .interfaces import generate_mapping, write_mock_data_to_ttl

# Instantiate the logger
logger = logging.getLogger(__name__)


def _try_index_from_str(s: str) -> Union[str, int]:
    try:
        return int(s)
    except ValueError:
        return s


def _nested_get(dic: Union[dict, list], keys: list[str]) -> Any:
    """Get a value of a JSON structured dict/list.

    Adapted from StackOverflow
    https://stackoverflow.com/questions/14692690/access-nested-dictionary-items-via-a-list-of-keys

    Args:
        dic (dict | list): JSON structure.
        keys (list[str]): List path via keys and index to desired value. The type needs to agree with the expectations.

    Raises:
        KeyError: Key does not exist in dict.
        TypeError: Cannot navigate through other data types than dict or list.
        ValueError: Wrong type in keys.

    Returns:
        Any: Value at the desired path (key/index).
    """
    dicc = dic.copy()
    for key in keys:
        if isinstance(dicc, dict):
            dicc = dicc[key]
        elif isinstance(dicc, list):
            dicc = dicc[int(key)]
        else:
            raise TypeError(f"Wrong type in `dic`: '{type(dicc)}' for {dicc}")
    return dicc


def _nested_set(dic: Union[dict, list], keys: list[str], value: Any) -> None:
    """Set a value of a JSON structured dict/list.

    Adapted from StackOverflow
    https://stackoverflow.com/questions/14692690/access-nested-dictionary-items-via-a-list-of-keys

    Args:
        dic (dict | list): JSON structure.
        keys (list[str]): List path via keys and index to desired value. The type needs to agree with the expectations.
        value (Any): Value to be set.

    Raises:
        NotImplementedError: Invalid path in JSON structure or incompatible data structure for navigation.
    """
    for key in keys[:-1]:
        if isinstance(dic, dict):
            dic = dic.setdefault(key, {})
        elif isinstance(dic, list):
            # skipping creation, since we assume that we don't need to create new list entries
            idx = _try_index_from_str(key)
            assert isinstance(idx, int), "Only integer keys are supported for lists"
            dic = dic[idx]
        else:
            raise NotImplementedError(f"Not implemented for type {type(dic)}")

    if isinstance(dic, dict):
        dic[keys[-1]] = value
    elif isinstance(dic, list):
        dic[int(keys[-1])] = value


def _del_dict_path(d: dict, keys: list) -> None:
    if len(keys) == 1:
        del d[_try_index_from_str(keys[0])]
    else:
        _del_dict_path(d[_try_index_from_str(keys[0])], keys[1:])


def _path_to_keys(path: str) -> Optional[list[str]]:
    keys = path.lstrip("#/").rstrip("/").split("/")
    if keys == [""]:  # root path "#"
        return None
    else:
        return keys


def _list_key_paths(schema: Any, key: str, path: str = "#") -> list[str]:
    res = []
    if isinstance(schema, dict):
        for k, v in schema.items():
            if k == key:
                res.append(f"{path}")
            if isinstance(v, dict):
                more_res = _list_key_paths(v, key, f"{path}/{k}")
                if more_res:
                    res.extend(more_res)
            elif isinstance(v, list) and k in ["oneOf", "anyOf"]:
                for i, vv in enumerate(v):
                    more_res = _list_key_paths(vv, key, f"{path}/{k}/{i}")
                    if more_res:
                        res.extend(more_res)

    return res


def get_keys(data: dict) -> list:
    """Extract all key paths from the patient data

    Args:
        data (dict): patient data

    Returns:
        list: list of key paths
    """
    result = []
    for key, value in data.items():
        if isinstance(value, dict):
            new_keys = get_keys(data=value)
            for innerkey in new_keys:
                result.append(f"{key}/{innerkey}")
        elif isinstance(value, list):
            for i_obj, obj in enumerate(value):
                if not isinstance(obj, dict):
                    continue
                new_keys = get_keys(data=obj)
                for innerkey in new_keys:
                    result.append(f"{key}/{i_obj}/{innerkey}")
        else:
            result.append(key)
    return result


def extract_code_term_paths(
    property_map: dict,
    code_keys: set[str],
    key_path: str,
    in_array_field: bool = False,
    in_one_of: bool = False,
) -> None:
    """Extract Code/Terminology key paths

    Args:
        property_map (dict): map of the property
        code_keys (set): collection of key paths - object for alteration.
        in_array_field (bool, optional): field is boolean. Defaults to False.
        in_one_of (bool, optional): field is in oneOf. Defaults to False.
    """
    if property_map["type"] == "object":
        if "oneOf" in property_map:
            for element in property_map["oneOf"]:
                if not isinstance(element, dict):
                    raise TypeError(f"Expected dict in property_map['oneOf']: `{element}`")
                extract_code_term_paths(property_map=element, code_keys=code_keys, key_path=key_path)
        else:
            one_of_in_array = in_array_field and in_one_of
            if not isinstance(property_map["properties"], dict):
                raise TypeError(f"""Expected dict in property_map['properties']: `{property_map["properties"]}`""")
            for key, value in property_map["properties"].items():
                extract_code_term_paths(
                    property_map=value,
                    code_keys=code_keys,
                    key_path=key_path + "/" + key,
                    in_array_field=one_of_in_array,
                )
    elif property_map["type"] == "array":
        if "oneOf" in property_map["items"]:
            if not isinstance(property_map["items"], dict):
                raise TypeError(f"""Expected dict in property_map['items']: `{property_map["items"]}`""")
            for element in property_map["items"]["oneOf"]:
                extract_code_term_paths(
                    property_map=element, code_keys=code_keys, key_path=key_path, in_array_field=True, in_one_of=True
                )
        else:
            if not isinstance(property_map["items"], dict) and isinstance(property_map["items"]["properties"], dict):
                raise TypeError(f"""Expected dict in property_map['items']['properties']: `{property_map["items"]}`""")
            for key, value in property_map["items"]["properties"].items():
                extract_code_term_paths(
                    property_map=value, key_path=key_path + "/" + key, code_keys=code_keys, in_array_field=True
                )

    if "description" in property_map and property_map["description"] in [
        "Unique ID for the given IRI. String format follows convention: <coding_system>-<identifier>",
        "ID of SPHN Concept 'Code'",
    ]:
        # Supporting concepts define sourceConceptType and sourceConceptID by default therefore we expect those values to be there
        if not key_path.startswith("/supporting_concepts/"):
            code_keys.add(key_path.lstrip("/"))


def add_source_concept_id(key_path: Any, data: dict) -> dict:
    """Add 'sourceConceptID' to JSON data where necessary

    Args:
        key_path (Any): key path
        data (dict): data object

    Returns:
        dict: data with added `sourceConceptID` at applicable `key_paths`.
    """
    current = data.copy()
    extracted_keys = str(key_path).split("/")
    nested_dict = _nested_get(current, extracted_keys[:-1])
    host_id_dict = _nested_get(current, extracted_keys[:-2])
    if isinstance(host_id_dict, list):
        # logger.warning(f"Multiple codes for {extracted_keys}")
        host_id_dict = _nested_get(current, extracted_keys[:-3])
    assert isinstance(host_id_dict, dict), f"Wrong type: {extracted_keys}\n{host_id_dict}"
    assert "id" in host_id_dict.keys(), f"Nested dict requires ID: {host_id_dict.keys()}"
    if key_path.startswith("supporting_concepts/sphn:Interpretation/") and "target_concept" in nested_dict:
        if isinstance(current, dict) and nested_dict["target_concept"] in [
            "https://biomedit.ch/rdf/sphn-schema/sphn#Terminology",
            "https://biomedit.ch/rdf/sphn-schema/sphn#Code",
        ]:
            _nested_set(current, extracted_keys[:-1] + ["sourceConceptID"], host_id_dict["id"])
    else:
        _nested_set(current, extracted_keys[:-1] + ["sourceConceptID"], host_id_dict["id"])
    if "sourceConceptID" not in _nested_get(current, extracted_keys[:-1]).keys():
        raise KeyError(f"Not in keys: sourceConceptID for {key_path}")
    return current


def extract_array_fields_map(data: dict) -> dict:
    """Extract a map {'property1': [{...}, ...], ...} with the properties in supporting concepts that are defined as arrays and the current values

    Args:
        data (dict): concept data

    Returns:
        dict: array fields map
    """
    array_map: dict = {}
    for key, value in data.items():
        if isinstance(value, list):
            if len(value) > 1:
                if key not in array_map:
                    array_map[key] = []
                for element in value:
                    array_map[key].append(element)
    return array_map


def find_longest_list_size(input_dict: dict) -> int:
    """Find the maximum length of array values for a concept

    Args:
        input_dict (dict): map of array fields

    Returns:
        int: maximum length
    """
    max_list_size = 0
    for value in input_dict.values():
        max_list_size = max(max_list_size, len(value))
    return max_list_size


def expand_supporting_array_fields(data: dict) -> None:
    """Expand supporting concepts array fields

    Args:
        data (dict): patient data; changed inplace.
    """
    if "supporting_concepts" in data:
        for concept_key, concept_list in data["supporting_concepts"].items():
            collected_items = []
            for distinct_concept in concept_list:
                array_map = extract_array_fields_map(data=distinct_concept)
                if array_map:
                    max_iteration = find_longest_list_size(input_dict=array_map)
                    iteration = 0
                    copied_el = distinct_concept
                    while iteration < max_iteration:
                        # Iterate as many times as the longest list of values for an array field in the distinct concept
                        for key, elements_list in array_map.items():
                            if iteration >= len(elements_list):
                                # In case the list of values is smaller than the maximum we replicate the first element
                                copied_el[key] = [elements_list[0]]
                            else:
                                copied_el[key] = [elements_list[iteration]]
                        if copied_el not in collected_items:
                            collected_items.append(copied_el)
                        iteration += 1
                elif distinct_concept not in collected_items:
                    collected_items.append(distinct_concept)
            data["supporting_concepts"][concept_key] = deepcopy(collected_items)


def pre_process(json_data: dict, json_schema: dict):
    # Push down sourceConcept ID and type
    code_term_paths: set = set()
    keys = get_keys(data=json_data)
    extract_code_term_paths(property_map=json_schema, code_keys=code_term_paths, key_path="")
    filtered_keys = [key for key in keys if re.sub("(/[0-9]*/)", "/", key) in code_term_paths]
    for key_path in filtered_keys:
        json_data = add_source_concept_id(key_path=key_path, data=json_data)
    # Expand array fields in supporting concepts
    expand_supporting_array_fields(data=json_data)


class SchemaBase:
    def __init__(self, schema: dict, mapping_file: None | Path = None) -> None:
        if isinstance(schema, dict):
            self.schema = schema
            self.input_schema = deepcopy(schema)
            self.mapping_file = mapping_file
        else:
            raise ValueError(
                f"'schema' needs to by of type dict, but is {type(schema)}. \
                Use 'Schema.from_file()' to load from a file."
            )

    def __repr__(self) -> str:
        return json.dumps(self.schema, indent=4)

    @classmethod
    def from_file(cls, filename: Path, data_provider_id: str | None = None):
        logger.info("Generating mapping.")
        json_schema, rml_mapping_file = generate_mapping(filename, data_provider_id)
        logger.debug(f"{rml_mapping_file=}")
        return cls(schema=json_schema, mapping_file=rml_mapping_file)

    @classmethod
    def from_json_file(cls, filename: Path):
        with open(filename, "r") as f:
            return cls(json.load(f))

    def __eq__(self, other) -> bool:
        return self.schema == other.schema

    def to_json_file(self, filename: str | Path) -> None:
        with open(filename, "w") as outfile:
            json.dump(self.schema, outfile, indent=4)

    # Path operations
    def get_from_path(self, path: str) -> Any:
        keys = _path_to_keys(path)
        if keys:
            return _nested_get(self.schema, keys)
        else:
            return self.schema

    def set_from_path(self, path: str, value: Any) -> None:
        keys = _path_to_keys(path)
        if keys:
            _nested_set(self.schema, keys, value)
        else:
            self.schema.update(value)

    def del_from_path(self, path: str) -> Any:
        keys = _path_to_keys(path)
        assert keys is not None, "Cannot delete root path."
        _del_dict_path(self.schema, keys)

    def get_key_paths(
        self,
        key: str,
        filter_func: Optional[Callable] = None,
        filter_parent: bool = False,
        root_path: str = "#",
        value: Any = None,
    ) -> list[str]:
        """Returns all paths (parents) that include a 'key' field

        Args:
            key: the key to search for.
            filter_func: if specified, only returns paths where filter_func(object) returns True.
            filter_parent: if True, returns the parent path of the key.
            root_path: if specified, restricts result below root_path
            value: if specified, restricts results to paths with the value

        Returns:
            list[str]: List of all parent paths that include the key.

        Note that it returns the parent paths not the full paths

        Example:
            >>> obj = SchemaBase(
            ...     schema={
            ...         "properties": {
            ...             "ob": {
            ...                 "properties": {
            ...                     "p1id": "x",
            ...                     "p1hu": "y"
            ...                 }
            ...             }
            ...         }
            ...     }
            ... )
            >>> obj.get_key_paths("properies")
            ["#", "#/properties/ob"]

        """
        try:
            search_schema = self.get_from_path(root_path)
        except KeyError:
            # root path not in schema
            return []

        paths = _list_key_paths(search_schema, key)

        # add root_path again
        paths = [p.lstrip("#/") for p in paths]
        paths = ["/".join([root_path, p]) for p in paths]
        paths = [p.rstrip("/") for p in paths]

        if filter_func:
            res = []
            for p in paths:
                filter_path = p if filter_parent else f"{p}/{key}"
                if self._run_filter_function(filter_path, filter_func):
                    res.append(p)
            paths = res

        if value:
            res = []
            for p in paths:
                value_path = p if filter_parent else f"{p}/{key}"
                schema_value = self.get_from_path(value_path)
                if schema_value == value:
                    res.append(p)
            paths = res

        # prepend root_path
        return paths

    def _run_filter_function(self, path: str, filter_func: Callable) -> Any:
        obj = self.get_from_path(path)
        try:
            return filter_func(obj)
        except Exception:
            return False

    def has_path(self, path: str) -> bool:
        try:
            self.get_from_path(path)
            return True
        except (KeyError, IndexError):
            return False


class MockData(SchemaBase):
    def to_file(self, filename: str | Path) -> None:
        if not self.mapping_file:
            raise RuntimeError("Cannot map without mapping file")

        filename = Path(filename).absolute()
        write_mock_data_to_ttl(
            mock_data_json=self.schema,
            mapping_file=self.mapping_file,
            output_file=filename,
        )


class Schema(SchemaBase):
    def get_dependencies(self, concepts: list[str]) -> list[str]:
        pattern = r"^SPHN Concept '([A-Za-z]+)'$"
        core_concepts_path = "#/properties/content/properties"
        core_concepts_schema = self.get_from_path(core_concepts_path)
        core_concepts = core_concepts_schema.keys()
        dependencies = []
        for concept in concepts:
            path = f"{core_concepts_path}/{concept}"
            schema = self.get_from_path(path)
            subpaths = get_keys(schema)
            for subpath in subpaths:
                if not subpath.endswith("description"):
                    continue
                description = self.get_from_path(f"{path}/{subpath}")
                match = re.match(pattern, description)
                if match:
                    dependent_concept = "sphn:" + match.group(1)
                    if dependent_concept in core_concepts:
                        dependencies.append(dependent_concept)
        return list(set(dependencies))

    def generate_data(
        self,
        context_faker: Faker | None = None,
        seed: Any = 0,
    ) -> MockData:
        Faker.seed(seed)

        # prepare faker
        if not context_faker:
            context_faker = Faker()

        # https://github.com/ghandic/jsf/blob/main/src/jsf/parser.py
        context = {"faker": context_faker}
        faker = JSF(self.schema, context=context)
        json_data = faker.generate()
        pre_process(json_data=json_data, json_schema=self.schema)
        data = MockData(json_data, mapping_file=self.mapping_file)

        return data

from pathlib import Path
from single_source import get_version

__version__ = get_version(__name__, Path(__file__).parent.parent)

# needs to exist inside `files/`
LATEST_DATA_TAG = "2025.1"

import logging
import shutil
import tempfile
from pathlib import Path
from pprint import pformat

import click
from faker import Faker

from . import (
    __version__,
    LATEST_DATA_TAG,
)
from .data_fetchers import (
    get_fixtures_dir,
    get_random_data_provider,
    get_schema_file,
    get_value_sets_dir,
)
from .data_stores import TabularDataStore, TreeDataStore
from .faker_providers import CustomData, CustomDataProvider
from .schema import Schema
from .schema_transformer import SchemaTransformerContent, SchemaTransformerStructure

# Create a custom logger
logger = logging.getLogger(__package__)
logger.setLevel(logging.INFO)
logger.propagate = False

# Create a console handler
consoleHandler = logging.StreamHandler()

# Configure the handler
log_fmt = "%(asctime)s %(levelname)-8s %(name)-35s %(message)s"
log_datefmt = "%Y-%m-%d %H:%M:%S"
consoleHandler.setFormatter(logging.Formatter(fmt=log_fmt, datefmt=log_datefmt))

# Add the handler to the logger
logger.addHandler(consoleHandler)


def _index_filename(f: Path, i: int, n: int) -> Path:
    stripped_name = f.with_suffix("").name
    suffix = str(i + 1).zfill(len(str(n)))
    return f.with_name(f"{stripped_name}_{suffix}{f.suffix}")


CONTEXT_SETTINGS = {"help_option_names": ["-h", "--help"]}
ALLOWED_OUTPUT_FILE_EXT = {".ttl", ".json"}


def _validate_items(min_items: int, max_items: int):
    """Validate the `min_items` and `max_items` arguments.

    Note: Problem with min > max

    Args:
        items (str, optional): _description_. Defaults to "1".

    Raises:
        ValueError: Invalid range format.
    """

    assert isinstance(min_items, int)
    assert isinstance(max_items, int)

    if min_items < 0:
        raise ValueError(f"min_items='{min_items}'<0.")
    if max_items < -1:
        raise ValueError(f"min_items='{min_items}'<-1.")
    if max_items != -1 and min_items > max_items:
        raise ValueError(f"min_items>max_items ({min_items}>{max_items})")


@click.command(
    help="Generate mock data according to a SPHN-compatible RDF schema.",
    context_settings=CONTEXT_SETTINGS,
)
@click.version_option(__version__)
@click.option(
    "-i",
    "--input-file",
    type=Path,
    default=None,
    help=f"""Input file in TTL-format to load the schema from. If none provided, uses the latest SPHN schema -
    `{LATEST_DATA_TAG}`.""",
    show_default=True,
)
@click.option(
    "-o",
    "--output-file",
    type=Path,
    default=None,
    help=f"File to write mock data to. If none provided, writes data to stdout. Formats: {ALLOWED_OUTPUT_FILE_EXT}",
    show_default=True,
)
@click.option(
    "-n",
    "--number-of-patients",
    type=click.IntRange(
        min=1,
        min_open=False,
    ),
    default=1,
    help="""Number of patients to create. If you specify an `output_file`, one file per patient will be created and an
    index will be appended to the filename.""",
    show_default=True,
)
@click.option(
    "-t",
    "--target-class",
    multiple=True,
    help="""Restrict mock data to a subset of classes. Each class needs its own flag. E.g.,
    `-t sphn:Allergy -t sphn:AllergyEpisode`
    """,
)
@click.option(
    "--sampled-data-dir",
    type=Path,
    default=None,
    help="""Directory which includes the sampled data. If `--input-file` is not provided, it will
    override the default. Note that this directory must include the sampled data in one or more JSON file(s). However,
    the directory must not include other JSON files.
    """,
)
@click.option(
    "--min-items",
    type=click.IntRange(min=0),
    default=1,
    help="""Minimum number of items to generate for each class.""",
    show_default=True,
)
@click.option(
    "--max-items",
    type=click.IntRange(min=-1),
    default=2,
    help="""Maximum number of items to generate for each class, but unlimited for special value -1. Detaults to 2.""",
    show_default=True,
)
@click.option(
    "--debug",
    is_flag=True,
    default=False,
    help="""More verbose output and intermediary output files written to the same directory as output file.""",
    show_default=True,
)
def cli(
    input_file: Path | None = None,
    output_file: Path | None = None,
    number_of_patients: int = 1,
    target_class: list[str] = [],
    sampled_data_dir: Path | None = None,
    min_items: int = 1,
    max_items: int = 2,
    debug: bool = False,
):
    f"""
    Generate mock data from an SPHN schema.

    Args:
        input_file (Path | None, optional): Input file in TTL-format to load the schema from.
            If none provided, uses the latest SPHN schema. Defaults to None, which refers to `{LATEST_DATA_TAG}`.
        output_file (Path | None, optional): File to write mock data to. If none provided, writes data to stdout.
            Supported formats: {ALLOWED_OUTPUT_FILE_EXT}. Defaults to None.
        number_of_patients (int, optional): Number of patients to create. If you specify an `output_file`,
            one file per patient will be created and an index will be appended to the filename. Defaults to 1.
        target_class (list[str], optional): Restrict mock data to a subset of classes. Each class needs its own flag.
            E.g., `["sphn:Allergy", "sphn:AllergyEpisode"]`. Defaults to [].
        sampled_data_dir (Path | None, optional): Directory which includes the sampled data.
            If `--input-file` is not provided, it will override the default. Note that this directory must include the
            sampled data in one or more JSON file(s). However, the directory must not include other JSON files.
            Defaults to None.
        min_items (int, optional): Minimum number of items to generate for each class. Defaults to 1.
        max_items (int, optional): Maximum number of items to generate for each class,
            but unlimited for special value -1. Defaults to 2.
        debug (bool, optional): More verbose logging information and intermediary output files written to the same
            directory as output file. Defaults to False.

    Raises:
        ValueError: Output file suffix must be one of {ALLOWED_OUTPUT_FILE_EXT}.
        ValueError: Invalid user input on argument `target_class`.
    """
    if output_file:
        suffix = output_file.suffix
        if suffix not in ALLOWED_OUTPUT_FILE_EXT:
            raise ValueError(f"Output file suffix must be one of {ALLOWED_OUTPUT_FILE_EXT}")

    if debug:
        logger.setLevel(logging.DEBUG)

    logger.info(f"Running {__name__}, version {__version__}")

    # Fallback input schema: 'sphn_mocker/files/{tag}/schema/*.ttl'
    schema_file = input_file if isinstance(input_file, Path) else get_schema_file(LATEST_DATA_TAG)
    if not schema_file.exists():
        raise FileNotFoundError(f"Schema file {schema_file} not found.")

    fixtures_dir = get_fixtures_dir(LATEST_DATA_TAG)
    if sampled_data_dir:
        value_sets_dir = sampled_data_dir
    else:
        value_sets_dir = get_value_sets_dir(LATEST_DATA_TAG)

    _validate_items(min_items=min_items, max_items=max_items)

    logger.debug(
        pformat(
            {
                "input_file": input_file,
                "output_file": output_file,
                "schema_file": schema_file,
                "fixtures_dir": fixtures_dir,
                "sampled_data_dir": sampled_data_dir,
                "value_sets_dir": value_sets_dir,
            }
        )
    )

    data_provider_id = get_random_data_provider(LATEST_DATA_TAG)
    logger.debug(f"{data_provider_id=}")
    schema = Schema.from_file(schema_file, data_provider_id)

    # Controlled error handling on user input `target_class`:
    invalid_targets = []
    for target in target_class:
        if target not in schema.input_schema["properties"]["content"]["properties"].keys():
            invalid_targets.append(target)
    if len(invalid_targets) > 0:
        str_invalid_targets = "`\n`".join(invalid_targets)
        raise ValueError(f"Invalid user input on argument `target_class`:\n`{str_invalid_targets}`")
    logger.debug(f"{target_class=}")

    logger.info("Transform schema.")
    TabularDataStore(fixtures_dir).load_all()
    TreeDataStore(value_sets_dir).load_all()

    if target_class:
        target_class = schema.get_dependencies(target_class)

    content_schema_tf = SchemaTransformerContent(schema)
    content_schema_tf.apply_all(filter_classes=target_class)

    structure_schema_tf = SchemaTransformerStructure(schema)
    structure_schema_tf.apply_all(schema_path=schema_file, min_items=min_items, max_items=max_items)

    faker = Faker()
    faker.add_provider(CustomDataProvider)

    for n in range(number_of_patients):
        # Reset the concept identifiers for each patient
        CustomData.concept_ids = {}

        logger.info(f"Generating patient data ({n+1}/{number_of_patients}).")
        data = schema.generate_data(context_faker=faker, seed=n)

        logger.debug("Writing output.")
        if output_file:
            parent_dir = output_file.absolute().parent
            parent_dir.mkdir(parents=True, exist_ok=True)
            indexed_output_file = (
                _index_filename(output_file, n, number_of_patients) if number_of_patients > 1 else output_file
            )
            indexed_output_pref = indexed_output_file.with_suffix("")
            if suffix == ".ttl":
                data.to_file(indexed_output_file)
            else:
                assert suffix == ".json", "Internal error: Output file suffix unchecked."
                data.to_json_file(indexed_output_file)

            # Write additional files for debugging
            if debug:
                logger.debug(f"Saving intermediary files to {indexed_output_pref}...")
                data.to_json_file(f"{indexed_output_pref}.json")
                schema.to_json_file(f"{indexed_output_pref}_transformed_schema.json")
                content_schema_tf.write_rules_to_file(
                    Path(f"{indexed_output_pref}_content_transformation_rules.json"), True
                )
                structure_schema_tf.write_rules_to_file(
                    Path(f"{indexed_output_pref}_structure_transformation_rules.json"), True
                )
                Schema(schema.input_schema).to_json_file(f"{indexed_output_pref}_input_schema.json")
                shutil.copy(schema.mapping_file, f"{indexed_output_pref}_mapping.ttl")
        else:
            # Write ttl-formatted output to stdout
            with tempfile.TemporaryDirectory() as tmp_dir:
                out_file = Path(tmp_dir) / "data.ttl"
                data.to_file(out_file)
                out_data = out_file.read_text()
            print(out_data)


if __name__ == "__main__":
    cli()

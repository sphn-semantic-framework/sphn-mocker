import json
import random
import re
from datetime import timedelta, date
from pathlib import Path
from typing import Optional, Union
import logging

from faker import Faker
import pandas as _pd

from .data_stores import Registry, TreeDataStore
from .schema import Schema
from .schema_constraints import get_schema_constraints

CORE_CONCEPT_PATH = "#/properties/content/properties"
SUPPORTING_CONCEPT_PATH = "#/properties/supporting_concepts/properties"
CHAINED_CODE_PROPERTIES = [
    "sphn:hasBodySite",
    "sphn:hasSubstance",
    "sphn:hasIntent",
    "sphn:hasDistance",
    "sphn:hasDuration",
    "sphn:hasQuantity",
    "sphn:hasUnit",
]
CHAINED_UNION_PROPERTIES = [
    "sphn:hasResult",
    "sphn:hasGeneticVariation",
]
PATH_MAPPING_RULES = {
    "sphn:ElectrocardiographicProcedure/sphn:hasSubjectPhysiologicState/sphn:hasCode": (
        "sphn:ElectrocardiographicProcedure",
        "sphn:hasSubjectPhysiologicState/sphn:hasCode",
    ),
    "sphn:RespiratoryRateMeasurement/sphn:hasResult/sphn:hasDataDetermination/sphn:hasMethodCode": (
        "sphn:RespiratoryRate",
        "sphn:hasDataDetermination/sphn:hasMethodCode",
    ),
    "sphn:hasDataDetermination/sphn:hasMethodCode": (
        "sphn:DataDetermination",
        "sphn:hasMethodCode",
    ),
}

# Instantiate the logger
logger = logging.getLogger(__name__)


def is_string(schema: dict) -> bool:
    return schema["type"] == "string"


def is_number(schema: dict) -> bool:
    return schema["type"] == "number"


def is_boolean(schema: dict) -> bool:
    return schema["type"] == "boolean"


def is_array(schema: dict) -> bool:
    return schema["type"] == "array"


def is_object(schema: dict) -> bool:
    return schema["type"] == "object"


def has_enum(schema: dict) -> bool:
    return "enum" in schema.keys()


def _mask_pattern(path: str, lookup_list: _pd.Series) -> _pd.Series:
    return lookup_list.apply(lambda x: re.search(x, path) is not None)


class SchemaTransformerBase:
    _schema: Schema
    _rules: dict[str, str | int] = {}

    def __init__(self, schema: Schema) -> None:
        self._schema = schema
        self._rules = {}

    def _get_concept_iri(self, path: str) -> Optional[str]:
        path = path.removesuffix("/properties")
        if self._schema.has_path(path):
            schema = self._schema.get_from_path(path)
            description = schema.get("description", "")
        else:
            return None
        match = re.search(r"^SPHN Concept '([\w]+)'$", description)
        if match:
            return "sphn:" + match.group(1)
        return None

    def _is_core_concept(self, concept_iri: str, path: str) -> bool:
        root_path = f"#/properties/content/properties/{concept_iri}/"
        has_nested_concepts = path.removeprefix(root_path).count("properties") > 1
        return path.startswith(root_path) and not has_nested_concepts

    def _get_core_concept_iris(self) -> list[str]:
        schemas = self._schema.get_from_path(CORE_CONCEPT_PATH)
        return list(schemas.keys())

    def _filter_core_concepts(self, concepts) -> None:
        schemas = self._schema.get_from_path(CORE_CONCEPT_PATH)
        filtered_schemas = {}
        for concept_iri in concepts:
            if concept_iri in schemas:
                filtered_schemas[concept_iri] = schemas[concept_iri]
        self._schema.set_from_path(CORE_CONCEPT_PATH, filtered_schemas)

    def _set_format(self, path: str, format: str) -> None:
        self._add_rule(path + "/format", format)

    def _set_provider(self, path: str, provider: str) -> None:
        self._add_rule(path + "/$provider", provider)

    def _add_rule(self, path: str, value: str | int) -> None:
        self._rules[path] = value

    def apply_rules(self) -> None:
        for path, value in self._rules.items():
            self._schema.set_from_path(path, value)

    def get_rules(self, sorted_by_path: bool = False) -> dict[str, str | int]:
        if sorted_by_path:
            return {path: value for path, value in sorted(self._rules.items())}
        return self._rules

    def write_rules_to_file(self, filepath: Path, sorted_by_path: bool = False) -> None:
        with open(filepath, "w") as output_file:
            json.dump(self.get_rules(sorted_by_path), output_file, indent=4)


class SchemaTransformerContent(SchemaTransformerBase):
    def __init__(self, schema: Schema) -> None:
        super().__init__(schema=schema)

    def _extract_relationship(self, path: str) -> tuple[str, str]:
        def skip_class(segments: list[str]) -> bool:
            if len(segments) < 2:
                return False
            if segments[-1] in CHAINED_CODE_PROPERTIES:
                return True
            if segments[-2] in CHAINED_UNION_PROPERTIES:
                return True
            return False

        class_iri: str | None = ""
        property_iri = ""

        segments = re.findall(r"(sphn:[\w]+)", path)
        if len(segments) < 2:
            return "", ""

        shortened_path = "/".join(segments)

        for def_path, (def_class_iri, def_property_iri) in PATH_MAPPING_RULES.items():
            if shortened_path.endswith(def_path):
                class_iri = def_class_iri
                property_iri = def_property_iri
                break

        if not class_iri:
            for _ in range(len(segments)):
                if (len(segments) < 2) or class_iri:
                    break
                separator = "/" if property_iri else ""
                segment = segments.pop()
                property_iri = segment + separator + property_iri
                path = path[: path.rfind(f"/{segment}")]
                class_iri = self._get_concept_iri(path)
                if skip_class(segments):
                    class_iri = ""
            if not class_iri:
                class_iri = segments.pop()

        assert isinstance(class_iri, str), f"Could not extract class IRI from path: {path}"
        return class_iri, property_iri

    def set_terminology_or_code(self) -> None:
        dataset = TreeDataStore()
        paths = self._schema.get_key_paths("description", value="SPHN Concept 'Code'/SPHN Concept 'Terminology'")
        for path in paths:
            class_iri, property_iri = self._extract_relationship(path)
            if dataset.get_random_value(class_iri, property_iri):
                target_description = "SPHN Concept 'Code'"
            else:
                target_description = "SPHN Concept 'Terminology'"
            target_paths = self._schema.get_key_paths("description", value=target_description, root_path=path)
            for target_path in target_paths:
                self._schema.del_from_path(target_path)

    def set_id_provider(self) -> None:
        segment = "id"
        core_concepts = []
        paths = self._schema.get_key_paths(segment, filter_func=is_string)
        for path in paths:
            concept_iri = self._get_concept_iri(path)
            if concept_iri:
                is_core_concept = self._is_core_concept(concept_iri, path)
                if is_core_concept:
                    core_concepts.append(concept_iri)
                arguments = f"'{concept_iri}', {is_core_concept}"
            else:
                arguments = ""
            path += "/" + segment
            self._set_provider(path, f"lambda: faker.id({arguments})")
        registry = Registry()
        registry.set_item("core_concepts", list(dict.fromkeys(core_concepts)))

    def set_identifier_provider(self) -> None:
        segment = "sphn:hasIdentifier"
        paths = self._schema.get_key_paths(segment)
        for path in paths:
            path += "/" + segment
            provider = "lambda: faker.bothify('ID-########')"
            self._set_provider(path, provider)

    def set_termid_provider(self) -> None:
        segment = "termid"
        provider = "lambda: faker.bothify('UNKNOWN-########')"
        paths = self._schema.get_key_paths(segment)
        for path in paths:
            self._set_provider(f"{path}/{segment}", provider)

    def set_iri_provider(self) -> None:
        segment = "iri"
        paths = self._schema.get_key_paths(segment, filter_func=lambda s: not has_enum(s))
        for path in paths:
            class_iri, property_iri = self._extract_relationship(path=path)
            path = path[: path.rfind("/")]
            provider = f"lambda: faker.terminology('{class_iri}', '{property_iri}')"
            self._set_provider(path, provider)

    def set_iri_format(self) -> None:
        segment = "iri"
        paths = self._schema.get_key_paths(segment, filter_func=lambda s: not has_enum(s))
        for path in paths:
            self._add_rule(f"{path}/{segment}/format", "iri")

    def set_datetime_ranges(self) -> None:
        faker = Faker()
        datetime_ranges: list[tuple[str, str, float, float]] = [
            ("sphn:hasStartDateTime", "sphn:hasEndDateTime", 2, 10),
            ("sphn:hasAdmissionDateTime", "sphn:hasDischargeDateTime", 2, 100),
        ]
        for datetime_range in datetime_ranges:
            start_property, end_property, min_interval, max_interval = datetime_range
            paths = self._schema.get_key_paths(start_property)
            for path in paths:
                start_path = path + "/" + start_property
                end_path = path + "/" + end_property
                if not self._schema.has_path(end_path):
                    continue
                random_date: date = faker.date_this_century()  # type: ignore[assignment]
                start_date_from = random_date.isoformat()
                start_date_to = (random_date + timedelta(days=1)).isoformat()
                end_date_from = (random_date + timedelta(days=min_interval)).isoformat()
                end_date_to = (random_date + timedelta(days=max_interval)).isoformat()
                self._set_provider(start_path, f"lambda: faker.timestamp('{start_date_from}', '{start_date_to}')")
                self._set_provider(end_path, f"lambda: faker.timestamp('{end_date_from}', '{end_date_to}')")

    def set_age_value_provider(self) -> None:
        paths = self._schema.get_key_paths("description", value="SPHN Concept 'Age'")
        for path in paths:
            path += "/properties/sphn:hasQuantity/properties/sphn:hasValue"
            if self._schema.has_path(path):
                self._set_provider(path, "lambda: faker.random_int(1, 100)")

    def apply_all(self, filter_classes: list[str] | None = None):
        if filter_classes:
            self._filter_core_concepts(filter_classes)
        self.set_terminology_or_code()
        self.set_id_provider()
        self.set_identifier_provider()
        self.set_termid_provider()
        self.set_iri_provider()
        self.set_iri_format()
        self.set_datetime_ranges()
        self.set_age_value_provider()
        self.apply_rules()


class SchemaTransformerStructure(SchemaTransformerBase):
    def __init__(self, schema: Schema) -> None:
        super().__init__(schema=schema)

    def enforce_concepts(self, enforce_proportion: float) -> None:
        if not 0 < enforce_proportion <= 1:
            raise ValueError(f"'enforce_proportion' needs to be between 0 and 1, but {enforce_proportion} specified.")

        # get list of content properties and select proportion
        prop_key = "properties"
        properties_paths = self._schema.get_key_paths(prop_key)
        for prop in properties_paths:
            # check for required field
            required_path = f"{prop}/required"
            try:
                req = self._schema.get_from_path(required_path)
            except KeyError:
                req = []

            # get keys not in required
            content = self._schema.get_from_path(f"{prop}/{prop_key}")
            content_keys = set(content.keys())
            remaining_keys = content_keys - set(req)
            n_keys = len(remaining_keys)

            # select subsample
            additional_keys = random.sample(list(remaining_keys), int(enforce_proportion * n_keys))
            additional_keys.sort()

            # set required field
            req.extend(additional_keys)
            self._schema.set_from_path(required_path, req)

    def filter_oneof_subschemas(self) -> None:
        pattern = r"^SPHN Concept '([A-Za-z]+)'$"
        required_core_concepts = self._schema.get_from_path("#/properties/content/required")
        oneof_paths = [path for path in self._schema.get_key_paths("oneOf") if path.startswith(SUPPORTING_CONCEPT_PATH)]
        for path in oneof_paths:
            subschemas = self._schema.get_from_path(f"{path}/oneOf")
            filtered_subschemas = []
            for subschema in subschemas:
                description = subschema.get("description", "")
                match = re.match(pattern, description)
                if match:
                    concept_iri = "sphn:" + match.group(1)
                    if concept_iri in required_core_concepts:
                        filtered_subschemas.append(subschema)
            if filtered_subschemas:
                self._schema.set_from_path(f"{path}/oneOf", filtered_subschemas)
            else:
                concept_iri = path.removeprefix(SUPPORTING_CONCEPT_PATH).split("/")[1]
                concept_schema_path = f"{SUPPORTING_CONCEPT_PATH}/{concept_iri}"
                if self._schema.has_path(concept_schema_path):
                    self._schema.del_from_path(concept_schema_path)

    def set_cardinalities(self, schema_path: Path, min_items: int = 0, max_items: Union[int, None] = 2) -> None:
        """Define cardinality constraints for all concepts.

        Args:
            schema_path (pathlib.Path): path to RDF schema in ttl-format to extract the cardinalities from there.
            min_items (int, optional): `minItems` for each concept. Defaults to 0.
            max_items (int | None, optional): `maxItems` for each concept. Defaults to 2.

        Returns:
            None
        """
        min_items = int(min_items)
        if min_items < 0:
            raise ValueError("`n_min_instances` cannot be < 0")
        if max_items is not None:
            max_items = int(max_items)
            if (max_items != -1) and (max_items < min_items):
                raise ValueError("`n_min_instances` cannot be smaller than `n_max_instances` if specifically -1.")

        # get all array paths
        paths = self._schema.get_key_paths("type", filter_func=is_array, filter_parent=True)

        # %% Get the schema constraints for each path
        cardinalities_schema_all = get_schema_constraints(schema_file=schema_path)
        # Select cardinalities in the RDF schema including only the ones not filtered correctly by the definition of
        # JSON-schema concept array; i.e., min cardinality >= 1 and max cardinality > 1 (or -1 meaning no max)
        cardinalities_schema = cardinalities_schema_all[
            (cardinalities_schema_all["min_cardinality"] >= 1) & (cardinalities_schema_all["max_cardinality"] != 1)
        ]
        # Create regexp pattern to search appearences:
        # Direct or nested parent concept (array or object), allow pattern only as matching end
        cardinalities_schema = cardinalities_schema.assign(
            pattern=cardinalities_schema["class"].str.replace(":", ":(has|)")
            + "/(items/|)properties/"
            + cardinalities_schema["property"]
            + "$"
        )

        # %% Set minItems and maxItems according to input for each path or according to the schema constraints
        for path in paths:
            # Mask (boolean) restricting cardinalities for the path
            path_constraint_schema = _mask_pattern(path, cardinalities_schema["pattern"])

            # Set minItems maxItems if there are no constraints from schema
            if not path_constraint_schema.any():
                concept_min_cardinality = min_items
                self._add_rule(f"{path}/minItems", concept_min_cardinality)
                if (max_items is not None) and (max_items > -1):
                    concept_max_cardinality = max_items
                    self._add_rule(f"{path}/maxItems", concept_max_cardinality)
                continue
            matching_cardinality = cardinalities_schema[path_constraint_schema]
            if len(matching_cardinality.index) > 1:
                raise ValueError(f"Multiple competing options:\n{matching_cardinality}")

            # Decide between user input and schema constraint (schema_min_cardinality)
            # Extract schema cardinalities
            schema_min_cardinality = int(matching_cardinality["min_cardinality"].values[0])
            schema_max_cardinality = int(matching_cardinality["max_cardinality"].values[0])

            # Set minItems
            # Minimum lower bounded by schema minimum
            if schema_min_cardinality > min_items:
                logger.warning(f"Set {path}/minItems={schema_min_cardinality}, despite the user input.")
                concept_min_cardinality = schema_min_cardinality
            # Minimum upper bounded by schema maximum
            elif 0 < schema_max_cardinality < min_items:
                logger.warning(f"Set {path}/minItems={schema_min_cardinality}, despite the user input.")
                concept_min_cardinality = schema_max_cardinality
            else:
                concept_min_cardinality = min_items
            self._add_rule(f"{path}/minItems", concept_min_cardinality)

            # Set maxItems
            if (max_items is not None) and (max_items > -1):
                # Maximum lower bounded by concept minimum
                if concept_min_cardinality > max_items:
                    logger.warning(f"Set {path}/maxItems={concept_min_cardinality}, as it was smaller than minItems.")
                    concept_max_cardinality = concept_min_cardinality
                # Maximum upper bounded by schema maximum
                elif (
                    (schema_max_cardinality is not None)
                    and (schema_max_cardinality > -1)
                    and (schema_max_cardinality < max_items)
                ):
                    concept_max_cardinality = schema_max_cardinality
                else:
                    concept_max_cardinality = max_items
                self._add_rule(f"{path}/maxItems", concept_max_cardinality)
            elif schema_max_cardinality > -1:
                concept_max_cardinality = schema_max_cardinality
                self._add_rule(f"{path}/maxItems", concept_max_cardinality)

    def set_instance_count(self) -> None:
        registry = Registry()
        core_concepts = registry.get_item("core_concepts") or []
        for concept_iri in core_concepts:
            path = f"#/properties/content/properties/{concept_iri}"
            data_type = self._schema.get_from_path(f"{path}/type")
            if data_type != "array":
                continue
            self._add_rule(f"{path}/minItems", 1)
            self._add_rule(f"{path}/maxItems", 1)

    def apply_all(
        self,
        schema_path: Path,
        min_items: int,
        max_items: Union[int, None],
    ):
        # enforce all concepts, as the connector does not apply restrictions multiple levels which can result in
        # violations for nested concepts.
        # see https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/issues/124
        self.enforce_concepts(1)
        self.filter_oneof_subschemas()
        self.set_cardinalities(schema_path, min_items, max_items)
        self.set_instance_count()
        self.apply_rules()

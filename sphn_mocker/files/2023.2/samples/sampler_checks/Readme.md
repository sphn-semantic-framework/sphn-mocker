Test cases:

* `strict`: descendants of the given classes are not allowed:
    * first level: `sphn:AdministrativeGender`
    * third level: `sphn:CircumferenceMeasure`
    * third level: `sphn:TumorSpecimen`

* `relaxed`: descendants of the given classes are allowed:
    * `sphn:AccessDevicePresence`

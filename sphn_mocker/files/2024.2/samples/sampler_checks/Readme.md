Test cases:

* `strict`: descendants of the given classes are not allowed:
    * first level: `sphn:AdministrativeSex`
    * second level: `sphn:CircumferenceMeasurement`
    * third level: `sphn:TumorSpecimen`

* `relaxed`: descendants of the given classes are allowed:
    * `sphn:BloodPressureMeasurement`
    * `sphn:Unit`

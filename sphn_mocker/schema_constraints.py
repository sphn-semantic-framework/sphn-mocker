import re
from pathlib import Path

from rdflib import Graph
from rdflib.namespace import NamespaceManager
from rdflib.query import Result as QueryResult
import pandas as pd


def is_url(text: str) -> bool:
    """Check whether a string is a valid URL.

    Args:
        text (str): Text to be checked.

    Returns:
        bool: Decision if string is a valid URL.
    """
    return re.match(r"^https?://\S+", text) is not None


def serialize_result_set(result_set, ns_manager: NamespaceManager, normalize_uris: bool = True):
    """Serialize a SPARQL query result to a list of lists.

    Args:
        result_set (rdflib.query.Result): SPARQL query result
        ns_manager (NamespaceManager): RDF namespace manager
        normalize_uris (bool, optional): Option to normalize URIs. Defaults to True.

    Returns:
        list: serialized result
    """
    serialized_result_set = []
    for row in result_set:
        serialized_row = []
        for value in row:
            value = value.toPython()
            if is_url(value) and normalize_uris:
                value = ns_manager.normalizeUri(value)
            serialized_row.append(value)
        serialized_result_set.append(serialized_row)
    return serialized_result_set


def result_set_to_dataframe(
    query_result: QueryResult, ns_manager: NamespaceManager, normalize_uris: bool = True
) -> pd.DataFrame:
    """Convert a SPARQL query result to a Pandas DataFrame.

    Args:
        query_result (rdflib.query.Result): SPARQL query result
        ns_manager (NamespaceManager): RDF namespace manager
        normalize_uris (bool, optional): Option to normalize URIs. Defaults to True.

    Returns:
        pandas.DataFrame: DataFrame with all query results.
    """
    result_set = serialize_result_set(query_result, ns_manager, normalize_uris)
    if query_result.vars is None:
        raise ValueError("Query result does not contain variables.")
    columns = [str(var) for var in query_result.vars]
    return pd.DataFrame(result_set, columns=columns)


def get_cardinality_constraints(graph: Graph, ns_manager: NamespaceManager) -> pd.DataFrame:
    """Get the cardinality constraints from a SPARQL query result.

    Args:
        graph (Graph): RDF graph
        ns_manager (NamespaceManager): RDF namespace manager

    Returns:
        pandas.DataFrame: DataFrame with all cardinality constraints.
    """
    query = """
        SELECT ?class ?property
            (STR(MAX(COALESCE(?minCardinality, -1))) AS ?min_cardinality)
            (STR(MAX(COALESCE(?maxCardinality, -1))) AS ?max_cardinality)
        WHERE {
            {
                ?class rdfs:subClassOf ?individualRestriction.
                ?individualRestriction rdf:type owl:Restriction.
                ?individualRestriction owl:onProperty ?property.
            }
            UNION
            {
                ?class (rdfs:subClassOf/owl:intersectionOf) ?restrictionList.
                ?restrictionList ((rdf:rest*)/rdf:first) ?individualRestriction.
                ?individualRestriction owl:onProperty ?property.
            }
            OPTIONAL { ?individualRestriction owl:minCardinality ?minCardinality. }
            OPTIONAL { ?individualRestriction owl:maxCardinality ?maxCardinality. }
        }
        GROUP BY ?class ?property
        ORDER BY ?class ?property
    """
    query_dtypes = {"class": "str", "property": "str", "min_cardinality": "int", "max_cardinality": "int"}

    query_result = graph.query(query)
    parsed_result = result_set_to_dataframe(query_result, ns_manager)
    if set(parsed_result.columns) != set(query_dtypes.keys()):
        raise KeyError(
            f"Query results in inconsistent data types: {set(query_dtypes.keys())}\nExpected: {set(parsed_result.columns)}"
        )
    return parsed_result.astype(query_dtypes)


def get_schema_constraints(schema_file: str | Path) -> pd.DataFrame:
    """Get the schema constraints from a schema file.

    Args:
        schema_file (str | Path): Path to the schema file.

    Returns:
        pandas.DataFrame: Data frame with only the relevant constraints.
    """
    graph = Graph(bind_namespaces="core")
    graph.parse(schema_file)
    ns_manager = NamespaceManager(graph)

    result = get_cardinality_constraints(graph, ns_manager)

    return result

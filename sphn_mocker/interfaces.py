import json
import os
import subprocess
import logging
from pprint import pformat

import tempfile
from sysconfig import get_path
from pathlib import Path
from typing import Tuple

# sphn_connector
from lib.rml_generator import main as rml_generator

# Instantiate the logger
logger = logging.getLogger(__name__)


def generate_mapping(
    schema_file: Path,
    data_provider_id: str | None = None,
    input_data: str | None = "patient_data_input.json",
) -> Tuple[dict, Path]:
    temp_dir = Path(tempfile.gettempdir())

    json_schema_file = temp_dir / "json_schema.json"
    rml_mapping_file = temp_dir / "rml_generated_mapping.ttl"

    if not data_provider_id:
        data_provider_id = "DATAPROVIDER"

    logger.debug("Running the rml_generator.")
    args = {
        "input_data": input_data,
        "schema_file": schema_file,
        "json_schema_output_path": json_schema_file,
        "rml_mapping_output_path": rml_mapping_file,
        "data_provider_id": data_provider_id,
        "project_name": None,
    }
    logger.debug(pformat(args))
    rml_generator(**args)

    with json_schema_file.open(encoding="UTF-8") as fi:
        json_schema = json.load(fi)

    json_schema_file.unlink()

    # remove  simplified.ttl
    filename = schema_file.with_suffix("").name
    simplified_ttl = schema_file.parent / f"{filename}_simplified.ttl"
    simplified_ttl.unlink()

    return json_schema, rml_mapping_file


def check_java_installed():
    try:
        subprocess.run(["java", "-version"], capture_output=True, check=True)
        # Java is installed
    except FileNotFoundError as e:
        raise RuntimeError("Java is not installed! Please install it.") from e


def write_mock_data_to_ttl(mock_data_json: dict, mapping_file: Path, output_file: Path) -> None:
    lib_dir = Path(get_path("platlib")) / "lib"
    jar_file = lib_dir / "rmlmapper-6.2.1-r0-all.jar"
    script_file = lib_dir / "execute_rml_mapper.sh"

    check_java_installed()
    mock_data_filename = "mock_data.json"

    logger.debug("Write mock data.")

    # wrangle output paths
    output_dir = output_file.parent
    target_file = output_dir / Path(mock_data_filename).with_suffix(".ttl")  # output file from mapper

    # write json to file
    input_dir = Path(tempfile.mkdtemp())
    mock_data_json_file = input_dir / mock_data_filename
    with mock_data_json_file.open("w", encoding="UTF-8") as fi:
        json.dump(mock_data_json, fi)

    if not jar_file.is_file():
        raise FileNotFoundError(jar_file)
    if not script_file.is_file():
        raise FileNotFoundError(script_file)

    # on Windows (native), needs to have the form `c:/data/.../mock.ttl`, not `C:\data\...\mock.ttl`
    # https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/issues/129#note_41736
    output_dir_windows_safe = str(output_dir).replace("\\", "/")

    _, options_file_str = tempfile.mkstemp(prefix="mapper_options")
    options_file = Path(options_file_str)
    options_file_text = f"""mappingfile={mapping_file}
serialization=turtle
inputfolder={input_dir}
outputfolder={output_dir_windows_safe}
"""
    options_file.write_text(options_file_text)

    logger.debug("Running RML Mapper:")
    # '10': Nicola: "{java_heap_space – can be set to ‘10’}"
    java_opts = "-Xmx10g"
    cmd = f"{script_file} {java_opts} {jar_file} {options_file}"
    logger.debug(cmd)
    p = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
    )
    _, error = p.communicate()

    message = None
    logger.debug(f"rml-mapper return code: {p.returncode}")
    if p.returncode != 0:
        message = f"Execution of rml-mapper failed for patient {mapping_file} {target_file}: {error.decode()}"

    if not os.path.exists(target_file):
        message = f"FAILED conversion. Output file '{target_file}' not successfully generated"
    elif os.path.getsize(target_file) == 0:
        message = f"FAILED conversion. Output file '{target_file}' is empty"
    else:
        logger.debug(f"SUCCESSFUL conversion. '{mapping_file}' converted to RDF file '{target_file}'")

    if message:
        raise RuntimeError(f"{message}\n\n{error.decode()}")

    logger.debug(f"Moving {target_file} to {output_file}")
    target_file.rename(output_file)

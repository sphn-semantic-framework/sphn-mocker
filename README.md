# SPHN Mocker

The SPHN Mocker generates mock data in JSON or Turtle format based on the SPHN RDF Schema.

Since real data is sensitive and cannot be shared directly for various use cases,
it can be challenging to understand what SPHN-compliant data looks like
without being involved in a specific SPHN project.

The SPHN Mocker addresses this issue by providing a way for users to see
how data might appear when compliant with the SPHN RDF Schema.
This helps alleviate some of the uncertainty and facilitates
testing of tools developed within the SPHN framework.

Currently, the generated mock data is used for:

- Testing the [SPHN Connector](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-connector) pipeline
- Facilitating demos, training sessions, and presentations with testable examples
- Enhancing the SPHN user experience (e.g., debugging queries by testing with mock data instead of real data)

## Compatibility

### SPHN RDF Schema

SPHN Mocker version 1.1.6 and newer are only compatible with the [SPHN RDF Schema 2024.2](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-schema/-/tags/2024-2-bugfix-cardinalities).
If you are using earlier versions of the SPHN RDF Schema, please use
SPHN Mocker [version 1.1.5](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/tags/1.1.5).

### Python version

Python 3.10 or later is required.

#### Compatibility issues with Python 3.12

You may encounter syntax warnings when using the tool for the first time.
These warnings are raised by Python 3.12 for invalid escape sequences originating from dependent packages.
Note that the syntax warnings are emitted by the compiler during code parsing
and do not affect the execution of the program at runtime.

Additionally, you may need to install Rust as it is required by certain dependencies.
For installation instructions and troubleshooting, refer to the
[official Rust documentation](https://www.rust-lang.org/tools/install).

### Operating system

SPHN Mocker can be installed on macOS, Linux, and Windows.

#### Installation on Windows

It is recommended to install the package on the Windows Subsystem for Linux (WSL),
specifically a Debian-based distribution like Ubuntu 22.04.
For installation instructions and troubleshooting, refer to the
[official WSL documentation](https://learn.microsoft.com/en-us/windows/wsl/install).

## Prerequisites

### Install Python and Java

The package requires Python and Java to be installed on the system.

### Create a virtual environment (optional)

It is recommended to install the package into a virtual environment, such as one created with
[`venv`](https://docs.python.org/3/library/venv.html) (as shown in the example)
or with [Conda](https://docs.conda.io/).

```bash
python -m venv .venv
source .venv/bin/activate
```

## Installation

The tool can be installed from source (via Poetry). It is also distributed as
a [Docker image](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/container_registry)
and as a [Python package](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/packages).

### Using pip

Ensure pip is installed and up-to-date:

```bash
python -m ensurepip --upgrade
```

Install the sphn-mocker package and its dependencies:

```bash
pip install sphn-mocker --index-url https://git.dcc.sib.swiss/api/v4/projects/629/packages/pypi/simple --extra-index-url https://git.dcc.sib.swiss/api/v4/projects/630/packages/pypi/simple --extra-index-url https://git.dcc.sib.swiss/api/v4/projects/474/packages/pypi/simple
```

Note that the `index-url` points to the registry hosting `sphn-mocker`
and the `extra-index-url` entries to the registries hosting `sphn-sampler`
and `sphn-connector` (dependencies of `sphn-mocker`).

Run the tool:

```bash
sphn-mocker -o mock_data.ttl
```

### Using Poetry

Clone the repository:

```bash
git clone https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker.git
cd sphn-mocker
```

Install the package:

```bash
poetry install
```

To run the tool, use `poetry run`:

```bash
poetry run sphn-mocker -o mock_data.ttl
```

### Using Docker

Pull the latest version of the Docker image:

```bash
docker pull registry.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker:latest
```

To run the tool, use `docker run`:

```bash
docker run --rm -it -v $PWD/data:/data sphn-mocker:latest -o /data/mock_data.ttl
```

- Creates and maps the `data/` directory from the current directory
  on the host to the `/data/` directory in the container.
- You can include any of the CLI options listed below.
  Just replace the CLI command `sphn-mocker` with your Docker run command (including all Docker options),
  e.g., `docker run --rm -it -v $PWD/data:/data sphn-mocker:latest`.

### Usage

```
$ sphn-mocker -h
Usage: sphn-mocker [OPTIONS]

  Generate mock data according to a SPHN-compatible RDF schema.

Options:
  --version                       Show the version and exit.
  -i, --input-file PATH           .ttl file to load the schema from. If none
                                  provided, uses the latest SPHN schema.
  -o, --output-file PATH          File to write mock data to. If none
                                  provided, writes data to stdout. Formats:
                                  {'.ttl', '.json'}
  -n, --number-of-patients INTEGER RANGE
                                  Number of patients to create. If you specify
                                  an `output_file`, one file per patient will
                                  be created and an index will be appended to
                                  the filename.  [default: 1; x>=1]
  -t, --target-class TEXT         Restrict mock data to a subset of classes.
                                  Each class needs its own flag. E.g.,  `-t
                                  sphn:Allergy -t sphn:AllergyEpisode`
  --sampled-data-dir PATH         Directory which includes the sampled data.
                                  If `--input-file` is not provided, it will
                                  override the default. Note that this
                                  directory must include the sampled data in
                                  one or more JSON file(s). However,  the
                                  directory must not include other JSON files.
  --debug                         More verbose output and intermediary output
                                  files written to the same directory as
                                  output file.
  -h, --help                      Show this message and exit.
```

## Generating mock data

Mock data can be generated with the command line interface (CLI) from the Python package or the Docker image.

### Quickstart

To generate data for the `latest` SPHN schema for one patient and print it to the terminal, run:

```bash
sphn-mocker
```

To store the output in a `.ttl` file, run:

```bash
sphn-mocker -o mock_data.ttl
```

To store the output in a `.json` file, run:

```bash
sphn-mocker -o mock_data.json
```

### Customizing restrictions samples

The tool uses a JSON file to determine possible values for restrictions.
A sample file can be generated with the
[`sphn-sampler`](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-sampler).
Default files with samples for the SPHN schema are included with the SPHN Mocker
and are used by default. If you need to customize the restrictions
(for example, to include data only from female patients),
you can use the default file as a template.
Edit it accordingly and pass it to the SPHN Mocker.

1. Create an empty directory on your disk.
1. Download an _sphn-sampler_ output file as a template (e.g.
   [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/blob/main/sphn_mocker/files/2024.2/samples/value_sets/sphn-sampler-data.json))
   and save it in this directory.
1. Open the JSON file and modify it according to your requirements.
1. Run the SPHN Mocker with the `--sampler-data-dir` option, specifying the path to your directory
   (e.g., `sphn-mocker --sampler-data-dir /path/to/json/dir [...]`

**Example edit**

The default files include the following restrictions for `AdministrativeSex`.

```json
    "sphn:AdministrativeSex": {
        "sphn:hasCode": [
            "http://snomed.info/id/248152002",
            "http://snomed.info/id/248153007",
            "http://snomed.info/id/32570681000036106"
        ]
    },
```

To restrict the mock data to values to only come from `female (248152002)`,
the template can be modified as follows:

```json
    "sphn:AdministrativeSex": {
        "sphn:hasCode": [
            "http://snomed.info/id/248152002"
        ]
    },
```

### Restrict data to a subset of classes

When generating data from the SPHN RDF Schema, all classes defined in that schema will potentially be generated.
To restrict the output to a subset of classes, use the `-t` or `--target-class` option.
For example, to limit the output to `sphn:Allergy` and `sphn:AllergyEpisode`, use:

```
sphn-mocker -t sphn:Allergy -t sphn:AllergyEpisode [...]
```

Note that you can only target core concepts and not sub-concepts instantiated as part of a core concept.
For more information see section on [schema processing](#schema-processing)

### Generate data for multiple patients

Use the `--number-of-patients` option to generate mock data for multiple patients, creating one file per patient.

### Python module usage as software developer

#### Load schema

```python
from sphn_mocker.schema import Schema

schema = Schema.from_file(schema_file)
# or
schema = Schema(dict)
```

#### Generate mock data (default, no customized data providers)

```python
data = schema.generate_data(faker)
```

#### Write data to file

```python
data.to_file(filename)
```

#### SPHN schemas and samples

##### Using schema and sample files

`sphn_mocker/files/{tag}/{schema, samples}/file.{ttl, json}` holds TTL schema files and JSON data sampler files.

```python
from sphn_mocker.data import get_schema_file, get_samples_file
filename = get_schema_file("latest") # returns latest schema
filename = get_schema_file("tag") # returns files/tag/schema/*.ttl
filename = get_samples_file("latest") # returns latest samples
filename = get_samples_file("tag") # returns files/tag/samples/*.json
```

##### Packaging schemas

1. Place the new schema file in the directory `sphn_mocker/files/tag/schema/`.
   Note that each subdirectory can only contain one `.ttl` file.
1. To update the reference to the latest schema, modify the `LATEST_DATA_TAG` variable in `sphn_mocker/__init__.py`.

### Add sampler data

- Create samples with the `sphn-sampler`,
  see [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-sampler#run-create-samples-for-sphn-mocker)
- Add the output to `sphn_mocker/files/tag/`


### Workflow

<div class="center">

```mermaid
graph LR
  %% inputs
  ext_ttl[ext_term.ttl]
  schema_ttl[schema.ttl]


  %% sphn-sampler
  sphn_sampler{sphn-sampler}
  ext_ttl --> sphn_sampler
  schema_ttl --> sphn_sampler

  sample_data_json[sample-data.json]
  sphn_sampler --> sample_data_json


  %% mapping_generator
  mapping_generator{mapping-generator}
  schema_ttl --> mapping_generator

  mapping_ttl[mapping.ttl]
  schema_json[schema.json]
  mapping_generator --> mapping_ttl
  mapping_generator --> schema_json

  %% transform schema
  schema_transformer{schema-transformer}
  sample_data_json --> schema_transformer
  schema_json --> schema_transformer
  schema_ttl --> schema_transformer
  transformed_schema[transformed-schema.json]
  schema_transformer --> transformed_schema

  %% fake data
  data_generator{data-generator}
  transformed_schema --> data_generator

  mock_data_json[mock-data.json]
  data_generator --> mock_data_json

  %% mapper
  mapper{mapper}
  mock_data_json --> mapper
  mapping_ttl --> mapper

  mock_data_ttl[mock-data.ttl]
  mapper --> mock_data_ttl

```

</div>

## Schema processing

After loading a given SPHN-compliant schema into an `rdflib` graph,
the `SPHN Connector` extracts all relevant concepts, which can be categorized into several types:

- Core concepts: Schema classes that define the object property `hasSubjectPseudoIdentifier`
  and thus connect directly to the `sphn:SubjecPseudoIdentifier` class.
  Additionally, `sphn:SourceSystem` is considered a core concept.
  In the JSON schema, these are defined at the top level under the content key.
  A complete list of the core concepts from the 2024.2 version of the SPHN RDF Schema
  can be found [here](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/blob/main/core_concepts.json).

- Special concepts: `sphn:SubjectPseudoIdentifier`, `sphn:DataProvider`, and `sphn:DataRelease`.

- Supporting concepts: Concepts that are neither core concepts nor referenced directly or indirectly within core concepts.
  In the JSON schema, these are defined at the top level under the supporting_concepts key.
  The 2024.2 version of the SPHN RDF Schema containts the following supporting concepts:
  - sphn:Interpretation
  - sphn:ReferenceInterpretation
  - sphn:SemanticMapping
  - sphn:SourceData


## License and Copyright

© Copyright 2025, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics

The SPHN Mocker is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
(see [License](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/blob/main/LICENSE)).

## Contact

For any question or comment, please contact the Data Coordination Center FAIR Data team at [fair-data-team@sib.swiss](mailto:fair-data-team@sib.swiss).

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/).

## [Unreleased]

## [1.2.1]

### Removed
- No more dependency to distutils

## [1.2.0]

### Changed
- Changed CLI options
- Updated sphn-sampler to `0.5.1`
- Updated sphn-connector-lib to `1.5.1`

### Fixed
- Fixed bugs

## [1.1.6]
### Added
- support for SPHN RDF Schema 2024.2

### Changed
- Updated sphn-connector-lib to `1.4.0`

## [1.1.5]
### Changed
- Updated sphn-sampler to `0.4.1`

## [1.1.4]
### Changed
- Updated sphn-connector-lib to `1.3.1.post2`

## [1.1.3]
### Added
- Added JSON output format

## [1.1.2]
### Fixed
- Fixed Windows path issue

## [1.1.1]
### Added
- Added Docker image

## [1.1.0]
### Added
- Logging messages for the terminal

### Changed
- renamed `--debug-write-intermediary-output` -> `--debug`
- updated sphn-connector-lib to `1.2.0`

## [1.0.0]

### Added
- Initial release with CLI

FROM python:3.11-buster AS builder

RUN pip install poetry
ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    POETRY_VIRTUALENVS_CREATE=1 \
    POETRY_CACHE_DIR=/tmp/poetry_cache

WORKDIR /code
COPY pyproject.toml poetry.lock ./
RUN touch README.md

# install dependencies only
RUN poetry install --only main  --no-root && rm -rf $POETRY_CACHE_DIR

# install code
COPY sphn_mocker ./sphn_mocker
RUN poetry install --only main  && rm -rf $POETRY_CACHE_DIR

# RUNTIME
FROM python:3.11-slim-buster AS runtime

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y default-jre && rm -rf /var/lib/apt/lists/*
ENV PATH="/code/.venv/bin:$PATH"

COPY --from=builder /code /code

ENTRYPOINT ["sphn-mocker"]

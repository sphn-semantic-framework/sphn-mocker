# Contributing to SPHN Mocker

## Reporting issues

When reporting issues please include as much detail as possible about your operating system, package versions and Python version.
Whenever possible, please also include a brief, self-contained code example that demonstrates the problem.


## Development

Start developing with Potry:

```bash
git clone https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker.git
cd sphn-mocker
poetry install
```

### How to add another GitLab project package registry

```bash
poetry source add --priority=supplemental gitlab_sampler https://git.dcc.sib.swiss/api/v4/projects/630/packages/pypi/simple
poetry add --source gitlab_sampler sphn_sampler
```

### How to update a poetry package (e.g., the `sphn_connector_lib`)

- Update manually `version` of a package in `pyproject.toml`:

```toml
sphn-connector-lib = {version = "^1.5.1", source = "gitlab_connector"}
```

- Apply the changes to your installation:

```bash
poetry lock
poetry install
```

- Commit and push changes.

### Release

When you have committed all changes and are ready to release the package:

- Add information about the release at the bottom of the relevant page and commit the changes.
- Use Poetry to bump the version (e.g., `poetry version patch`).
- Execute `tag_repo.sh`.
- Push the tags to trigger the package build and publication
  at [SPHN Mocker Packages](https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/packages).

```bash
poetry version patch # allowed strategies: patch, minor, major, prepatch, preminor, premajor, prerelease
bash tag_repo.sh
git push && git push --tags
```

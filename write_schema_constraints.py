import csv

from sphn_mocker.data_fetchers import get_schema_file
from rdflib import Graph, Namespace
from rdflib.namespace import NamespaceManager
from pathlib import Path

SPHN = Namespace("https://biomedit.ch/rdf/sphn-ontology/sphn#")

if __name__ == "__main__":
    schema_version = "2024.2"
    schema_filepath = get_schema_file(schema_version)
    output_dirpath = Path(f"sphn_mocker/files/{schema_version}/config")
    output_filepath = output_dirpath / "property_constraints.csv"

    graph = Graph(bind_namespaces="core")
    graph.parse(schema_filepath)
    ns_manager = NamespaceManager(graph)

    # Retrieve the property constraints
    query = """
    SELECT DISTINCT ?domain ?property ?range ?property_type
    WHERE {
        VALUES ?type { owl:DatatypeProperty owl:ObjectProperty }
        ?property a ?property_type.
        ?property (rdfs:domain/((owl:unionOf/(rdf:rest*)/rdf:first)*)) ?domain.
        ?property (rdfs:range/((owl:unionOf/(rdf:rest*)/rdf:first)*)) ?range.
        FILTER(ISIRI(?domain))
        FILTER(ISIRI(?range))
    }
    ORDER BY ?domain ?property ?range
    """
    query_result = graph.query(query)

    # Normalize the URLs in the results
    constraints = []
    for row in query_result:
        row = [ns_manager.normalizeUri(value.toPython()) for value in row]
        constraints.append(row)

    # Write the results to a CSV file
    with open(output_filepath, "w") as outfile:
        writer = csv.writer(outfile)
        writer.writerow([str(var) for var in query_result.vars])
        writer.writerows(constraints)

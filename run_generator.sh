#!/usr/bin/env bash
# Script Name: run_generator.sh
# Description: This script generates mockdata on several threads independently.
# Usage: ./run_generator.sh <arg1> <arg2>
# Arguments:
#   arg1: Number of threads.
#   arg2: Number of patients per thread.

# TODO: externalize output folder path

# Function to display usage information
usage() {
    echo "Usage: $0 <arg1> <arg2>"
    echo
    echo "Arguments:"
    echo "  arg1  Number of threads."
    echo "  arg2  Number of patients per thread."
    echo
    echo "Options:"
    echo "  --help  Display this help message."
}

run_mockGenerator() {
    num_thread=$1
    num_patients_per_thread=$2
    echo "Running docker command with thread $num_thread  and num_patients $num_patients_per_thread"
    docker run --name "mockdata-$num_thread" -it -d -v /mnt/raid/mockdata/$num_thread:/data registry.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker:latest -o /data/out.json -n $num_patients_per_thread
}

# Check if --help is provided
if [ "$1" == "--help" ]; then
    usage
    exit 0
fi

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
    echo "Wrong number of arguments ($#/2)."
    usage
    exit 1
fi

# this gets the max number of processes for the user
num_processes=$1
num_patients_per_thread=$2
for ((i=0;i<$num_processes;i++))
do
    run_mockGenerator "$i" "$num_patients_per_thread" &
done


# Wait some time to ensure containers are effectively created
sleep 0.1
echo ""

# Wait for containers to stop
for ((i=0;i<$num_processes;i++))
do
    echo "Waiting for process $i."
    docker container wait "mockdata-$i"
done;
echo ""
# echo "$(docker ps -a -f status=exited -f name=mockdata-*)"

# Write logs to files
echo "Write logs for debugging:"
for ((i=0;i<$num_processes;i++))
do
    mkdir -p logs
    echo "- logs/mockdata-$i.log"
    docker logs "mockdata-$i" > logs/mockdata-$i.log 2>&1
done
echo ""

echo "Removing exited containers:"
docker rm $(docker ps -a -q -f status=exited -f name="mockdata-*")

from pathlib import Path
import pytest
from faker import Faker

from sphn_mocker import LATEST_DATA_TAG
from sphn_mocker.data_fetchers import get_schema_file
from sphn_mocker.schema import (
    Schema,
    MockData,
    _path_to_keys,
    _try_index_from_str,
)
from sphn_mocker.schema_transformer import SchemaTransformerStructure, SchemaTransformerContent


@pytest.fixture
def schema():
    test_data_base_dir = Path(__file__).parent / "test_data"
    filename = test_data_base_dir / "json_schema_latest_2022_2_mini.json"
    schema = Schema.from_json_file(filename)
    return schema


@pytest.fixture
def schema_full():
    test_data_base_dir = Path(__file__).parent / "test_data"
    filename = test_data_base_dir / "20221207_json_schema.json"
    schema = Schema.from_json_file(filename)
    return schema


@pytest.fixture
def schema_2024():
    test_data_base_dir = Path(__file__).parents[1] / "sphn_mocker/files/2024.2/schema"
    filename = test_data_base_dir / "sphn_rdf_schema.json"
    schema = Schema.from_json_file(filename)
    return schema


@pytest.fixture
def utils_schema():
    test_data_base_dir = Path(__file__).parent / "test_data"
    filename = test_data_base_dir / "utils_schema.json"
    schema = Schema.from_json_file(filename)
    return schema


@pytest.fixture
def iri_schema():
    test_data_base_dir = Path(__file__).parent / "test_data"
    filename = test_data_base_dir / "iri_schema.json"
    schema = Schema.from_json_file(filename)
    return schema


class TestSchemaSetup:
    def test_schema_init(self):
        d = {"a": 1}
        schema = Schema(d)
        assert isinstance(schema, Schema)
        assert schema.schema == d

    def test_schema_load_from_file(self, schema):
        assert isinstance(schema, Schema)

    def test_schema_print(self, schema):
        print(schema)

    def test_write_file(self, tmp_path, schema):
        filename_out = Path(tmp_path) / "out.json"
        schema.to_json_file(filename_out)

        r = Schema.from_json_file(filename_out)
        assert schema == r


class TestSchemaGet:
    def test_get_properties(self, utils_schema):
        res = utils_schema.get_key_paths("properties")

        expected = [
            "#",
            "#/properties/ob",
            "#/sphn:DataRelease",
            "#/sphn:DataProviderInstitute",
            "#/sphn:DataProviderInstitute/properties/sphn:hasCode/oneOf/0",
            "#/sphn:DataProviderInstitute/properties/sphn:hasCode/oneOf/1",
        ]

        assert res == expected

    def test_get_properties_root_path(self, utils_schema):
        root_path = "#/sphn:DataProviderInstitute"
        res = utils_schema.get_key_paths("properties", root_path=root_path)

        expected = [
            "#/sphn:DataProviderInstitute",
            "#/sphn:DataProviderInstitute/properties/sphn:hasCode/oneOf/0",
            "#/sphn:DataProviderInstitute/properties/sphn:hasCode/oneOf/1",
        ]

        assert res == expected

    def test_get_key_paths(self, utils_schema):
        res = utils_schema.get_key_paths("id")

        expected = [
            "#/properties",
            "#/properties/ob/properties",
            "#/sphn:DataRelease/properties",
            "#/sphn:DataProviderInstitute/properties",
        ]

        assert res == expected

    def test_get_key_paths_oneof(self, utils_schema):
        res = utils_schema.get_key_paths("termid")

        expected = [
            "#/sphn:DataProviderInstitute/properties/sphn:hasCode/oneOf/1/properties",
        ]

        assert res == expected

    def test_filter_key_paths(self, utils_schema):
        def check_has_description_d(obj):
            return obj["description"] == "d"

        res = utils_schema.get_key_paths("id", filter_func=check_has_description_d)

        expected = [
            "#/sphn:DataProviderInstitute/properties",
        ]

        assert res == expected

    def test_get_key_value_paths(self):
        d = {"a": {"s": "blah", "l": [1, 2], "d": {"aa": 1, "bb": 2}}, "b": {"s": "blub"}, "z": "boring"}
        schema = Schema(d)

        # str
        res = schema.get_key_paths("s", value="blah")
        expected = ["#/a"]
        assert res == expected

        # list
        res = schema.get_key_paths("l", value=[1, 2])
        expected = ["#/a"]
        assert res == expected

        # dict
        res = schema.get_key_paths("d", value={"aa": 1, "bb": 2})
        expected = ["#/a"]
        assert res == expected

    def test_get_key_value_root_path_paths(self):
        d = {"a": {"s": "blah", "l": [1, 2], "d": {"aa": 1, "bb": 2}}, "b": {"s": "blah"}, "z": "boring"}
        schema = Schema(d)

        # str
        res = schema.get_key_paths("s", value="blah", root_path="#/a")
        expected = ["#/a"]
        assert res == expected

    def test_has_path(self):
        d = {"a": {"b": True, "c": [0, 1, 2]}}
        schema = Schema(d)
        assert schema.has_path("#/a") == True
        assert schema.has_path("#/z") == False

    def test_has_path_list(self):
        d = {"a": {"b": True, "c": [0, 1, 2]}}
        schema = Schema(d)
        assert schema.has_path("#/a/c/0") == True
        assert schema.has_path("#/a/c/99") == False

    def test_get_invalid_path(self):
        d = {"a": 1}
        schema = Schema(d)

        res = schema.get_key_paths("s")
        expected = []
        assert res == expected

    def test_get_invalid_path_value(self):
        d = {"a": 1}
        schema = Schema(d)

        res = schema.get_key_paths("s", value="blah")
        expected = []
        assert res == expected

    def test_get_invalid_path_value_root_path(self):
        d = {"a": 1}
        schema = Schema(d)

        # str
        res = schema.get_key_paths("s", value="blah", root_path="#/b")
        expected = []
        assert res == expected


class TestSchemaSet:
    def test_nested_set_from_path(self):
        d = {"k1": {"k2": "v2", "k3": "v3"}, "kk1": "vv1"}
        schema = Schema(d)
        schema.set_from_path("#/k0/k0/kkk0", "test")
        assert schema.get_from_path("#/k0/k0/kkk0") == "test"

    def test_nested_set_from_path_root(self):
        schema = Schema({})
        schema.set_from_path("#", {"k1": "test"})
        assert schema.get_from_path("#") == {"k1": "test"}

    def test_nested_set_from_path_list(self):
        d = {"k1": ["v1", "v2"], "kk1": "vv1"}
        schema = Schema(d)
        schema.set_from_path("#/k1/0", "test")
        assert schema.get_from_path("#/k1/0") == "test"
        assert schema.get_from_path("#/k1/1") == "v2"

    def test_nested_set_from_path_deep_list(self):
        d = {"k1": [{"kk1": "vv1"}, {"kk2": "vv2"}]}
        schema = Schema(d)
        schema.set_from_path("#/k1/0/kk1", "xx1")
        assert schema.get_from_path("#/k1/0/kk1") == "xx1"
        assert schema.get_from_path("#/k1/1/kk2") == "vv2"


class TestSchemaDel:
    def test_nested_del_from_path(self):
        d = {"k1": {"k2": "v2", "k3": "v3"}, "kk1": "vv1"}
        schema = Schema(d)
        schema.del_from_path("#/k1/k2")
        assert schema.schema == {"k1": {"k3": "v3"}, "kk1": "vv1"}

    def test_nested_del_from_path_list(self):
        d = {"k1": ["v1", "v2"], "kk1": "vv1"}
        schema = Schema(d)
        schema.del_from_path("#/k1/0")
        assert schema.schema == {"k1": ["v2"], "kk1": "vv1"}

    def test_nested_del_from_path_nested_list(self):
        d = {"k1": [{"v1": {"a": 1}}, {"v2": {"b": 2}}], "kk1": "vv1"}
        schema = Schema(d)
        schema.del_from_path("#/k1/0")
        assert schema.schema == {"k1": [{"v2": {"b": 2}}], "kk1": "vv1"}


class TestKeyUtils:
    def test_path_to_keys(self):
        assert ["properties", "ob"] == _path_to_keys("#/properties/ob")

    def test_path_to_keys_rootpath(self):
        assert _path_to_keys("#") is None

    def test_path_to_keys_trailing_slash(self):
        assert ["properties", "ob"] == _path_to_keys("#/properties/ob/")

    def test_nested_get_from_path(self):
        d = {"k1": {"k2": "v2", "k3": "v3"}, "kk1": "vv1"}
        assert Schema(d).get_from_path("#/k1/k2") == "v2"
        assert Schema(d).get_from_path("#/kk1") == "vv1"

    def test_nested_get_from_path_root(self):
        d = {"k1": {"k2": "v2", "k3": "v3"}, "kk1": "vv1"}
        assert Schema(d).get_from_path("#") == d

    def test_nested_get_from_path_lists(self):
        d = {"k1": ["v1", "v2"], "kk1": "vv1"}
        assert Schema(d).get_from_path("#/k1/0") == "v1"
        assert Schema(d).get_from_path("#/k1/1") == "v2"

    def test_try_index_from_str(self):
        assert _try_index_from_str("3") == 3
        assert _try_index_from_str("blah") == "blah"


class TestSetFormat:
    def test_set_iri_format(self, iri_schema):
        tf = SchemaTransformerContent(iri_schema)
        tf.set_iri_format()
        tf.apply_rules()

        res = iri_schema.get_from_path("#/sphn:DataRelease/properties/iri/format")
        assert res == "iri"

        res = iri_schema.get_from_path("#/sphn:DataProviderInstitute/properties/iri/format")
        assert res == "iri"

    def test_set_iri_enum_skipped(self, iri_schema):
        tf = SchemaTransformerContent(iri_schema)
        tf.set_iri_format()
        tf.apply_rules()

        res = iri_schema.get_from_path("#/sphn:hasComparator/properties/iri")
        assert "format" not in res.keys()


# class TestSet
class TestGenerate:
    def test_generate(self, schema):
        faker = Faker()
        data = schema.generate_data(faker)
        assert isinstance(data, MockData)

    def test_generate_no_faker(self, schema):
        data = schema.generate_data()
        assert isinstance(data, MockData)

    def test_enforce_proportion(self):
        schema = Schema({})
        v = {"type": "array"}
        for i in range(100):
            schema.set_from_path(f"#/properties/content/properties/prop_{i}", v)

        tf = SchemaTransformerStructure(schema)
        tf.enforce_concepts(0.7)

        required = schema.get_from_path("#/properties/content/required")
        assert len(required) == 70

    def test_enforce_proportion_small_data(self):
        schema = Schema({})
        v = {"type": "array"}
        for i in range(2):
            schema.set_from_path(f"#/properties/content/properties/prop_{i}", v)

        tf = SchemaTransformerStructure(schema)
        tf.enforce_concepts(0.7)

        required = schema.get_from_path("#/properties/content/required")
        assert len(required) == 1

    def test_enforce_proportion_nested(self):
        # generate schema
        # o=0: 50 items already in required
        # o=1: no required field

        schema = Schema({})
        v = {"type": "array"}
        for o in range(2):
            for i in range(100):
                schema.set_from_path(f"#/properties/content/properties/prop_{o}/properties/prop_{i}", {})
        o = 0
        req = [f"prop_{i}" for i in range(50)]
        schema.set_from_path(f"#/properties/content/properties/prop_{o}/required", req)

        tf = SchemaTransformerStructure(schema)
        tf.enforce_concepts(0.5)
        tf.apply_rules()

        o = 0
        required = schema.get_from_path(f"#/properties/content/properties/prop_{o}/required")
        assert len(required) == 75  # 0.5 applies to the remaining 50 entries-> 50 + 50*.5

        o = 1
        required = schema.get_from_path(f"#/properties/content/properties/prop_{o}/required")
        assert len(required) == 50  # 0.5 applies to the all 100 entries-> 100*.5

    def test_set_cardinalities_nomax(self):
        schema = Schema({})
        v = {"type": "array"}
        for i in range(2):
            schema.set_from_path(f"#/properties/content/properties/prop_{i}", v)

        n_min = 5
        tf = SchemaTransformerStructure(schema)
        tf.set_cardinalities(get_schema_file(LATEST_DATA_TAG), n_min, None)
        tf.apply_rules()

        res = schema.get_from_path(f"#/properties/content/properties/prop_0/minItems")
        assert res == n_min
        with pytest.raises(KeyError, match="maxItems"):
            schema.get_from_path(f"#/properties/content/properties/prop_0/maxItems")

    def test_set_cardinalities_neg(self, schema_2024):
        # https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/issues/7
        schema = schema_2024
        n_min = 6
        tf = SchemaTransformerStructure(schema)
        tf.set_cardinalities(
            get_schema_file(LATEST_DATA_TAG),
            min_items=n_min,
            max_items=-1,
        )
        tf.apply_rules()

        tf.write_rules_to_file("debug.txt")
        # Cardinality unconstraint by schema
        res = schema.get_from_path(f"#/properties/content/properties/sphn:ProblemCondition/minItems")
        assert res == n_min

        with pytest.raises(KeyError, match="maxItems"):
            schema.get_from_path(f"#/properties/content/properties/sphn:ProblemCondition/maxItems")

        # Cardinality example constraint by schema
        res1 = schema.get_from_path(
            f"#/properties/content/properties/sphn:AccessDevicePresence/items/properties/sphn:hasSourceSystem/minItems"
        )
        assert res1 == n_min

        with pytest.raises(KeyError, match="maxItems"):
            schema.get_from_path(
                f"#/properties/content/properties/sphn:AccessDevicePresence/items/properties/sphn:hasSourceSystem/maxItems"
            )

    def test_set_cardinalities_pos(self, schema_full):
        # https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-mocker/-/issues/7
        schema = schema_full
        n_min = 10
        n_max = 42

        tf = SchemaTransformerStructure(schema)
        tf.set_cardinalities(get_schema_file(LATEST_DATA_TAG), n_min, n_max)
        tf.apply_rules()

        assert schema.get_from_path(f"#/properties/content/properties/sphn:ProblemCondition/minItems") == int(n_min)
        assert schema.get_from_path(f"#/properties/content/properties/sphn:ProblemCondition/maxItems") == int(n_max)

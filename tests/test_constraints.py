from pathlib import Path

import pandas as pd

from sphn_mocker.schema_constraints import get_schema_constraints


def test_get_schema_constraints():
    """
    Test get_schema_constraints function for reproducibility
    """
    ref_data = pd.read_csv(Path(__file__).parent / "test_data/schema_constraints_2024-2.csv")
    data = get_schema_constraints(Path(__file__).parents[1] / "sphn_mocker/files/2024.2/schema/sphn_rdf_schema.ttl")
    pd.testing.assert_frame_equal(ref_data, data)

import pytest

from sphn_mocker.data_fetchers import (
    get_data_tags,
    get_fixtures_dir,
    get_random_data_provider,
    get_sampler_checks_file,
    get_schema_file,
    get_value_sets_dir,
    get_value_sets_file,
)


def test_get_schema_file():
    schema = get_schema_file("latest")
    assert schema.is_file()


def test_get_value_sets_file():
    samples = get_value_sets_file("latest")
    assert samples.is_file()


def test_get_data_tags():
    tags = get_data_tags()
    assert isinstance(tags, list)
    assert tags[0] == "latest"


def test_get_fixtures_dir():
    d = get_fixtures_dir("latest")
    assert d.is_dir()


def test_get_value_sets_dir():
    d = get_value_sets_dir("latest")
    assert d.is_dir()


def test_get_random_data_provider():
    dp = get_random_data_provider("latest")
    assert len(dp) == 15
    assert dp.startswith("CHE-")
    assert len(dp.split("-")) == 2  # format CHE-108_904_325
    print(dp)


@pytest.mark.skip("Unused function")
def test_get_sampler_checks_file():
    file = get_sampler_checks_file("latest")
    assert file.is_file()

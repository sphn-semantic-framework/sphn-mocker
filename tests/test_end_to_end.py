import pytest
from pathlib import Path
import tempfile
from rdflib import Graph

from sphn_mocker.schema import Schema, MockData
from sphn_mocker.data_fetchers import get_schema_file

test_data_base_dir = Path(__file__).parent / "test_data"


@pytest.fixture
def schema():
    schema_file = test_data_base_dir / "schema_mini.ttl"
    schema = Schema.from_file(schema_file)
    return schema


def test_generate_default(schema):
    data = schema.generate_data()
    assert isinstance(data, MockData)


def test_generate_args(schema):
    data = schema.generate_data()
    assert isinstance(data, MockData)


def test_write_file(schema):
    temp_dir = Path(tempfile.gettempdir())
    out_file = temp_dir / "mock_data.ttl"

    data = schema.generate_data()
    data.to_file(out_file)

    assert out_file.is_file()

    try:
        g = Graph()
        g.parse(out_file)
        assert len(g) > 0
    finally:
        out_file.unlink()

import json
from pathlib import Path

import pytest
from click.testing import CliRunner

from sphn_mocker import __version__, cli

test_data_base_dir = Path(__file__).parent / "test_data"


class TestBasics:
    def test_cli_help(self):
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["-h"])
        assert result.exit_code == 0
        assert "Generate mock data" in result.output

    def test_cli_version(self):
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["--version"])
        assert result.exit_code == 0
        assert __version__ in result.output

    @pytest.mark.skip("Too resource intense.")
    def test_cli_no_args(self):
        result = cli.cli()
        assert result is None


class TestOptionChecks:
    def test_cli_superfluous_arg(self):
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["arg"])
        assert result.exit_code != 0


class TestOutFiles:
    def test_one_patient_out_file(self, tmp_path):
        schema_ttl = test_data_base_dir / "schema_mini.ttl"

        out_file = tmp_path / "mock_data.ttl"
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["-i", schema_ttl, "-o", out_file])
        assert result.exit_code == 0
        assert out_file.is_file()

    def test_one_patient_out_file_json(self, tmp_path):
        schema_ttl = test_data_base_dir / "schema_mini.ttl"

        out_file = tmp_path / "mock_data.json"
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["-i", schema_ttl, "-o", out_file])
        assert result.exit_code == 0
        assert out_file.is_file()
        with open(out_file, "r") as json_file:
            data = json.load(json_file)
            assert len(data) > 0

    def test_multi_patient_out_file(self, tmp_path):
        schema_ttl = test_data_base_dir / "schema_mini.ttl"

        out_file = tmp_path / "mock_data.ttl"
        out_file_1 = tmp_path / "mock_data_1.ttl"  # should be created
        out_file_2 = tmp_path / "mock_data_2.ttl"  # should be created
        out_file_3 = tmp_path / "mock_data_3.ttl"  # should not be created

        runner = CliRunner()

        result = runner.invoke(cli.cli, ["-i", schema_ttl, "-o", out_file, "-n", "2"])
        assert result.exit_code == 0

        assert out_file_1.is_file()
        assert out_file_2.is_file()

        assert not out_file.is_file()
        assert not out_file_3.is_file()


class TestDataOptions:
    def test_filter_class(self):
        schema_ttl = test_data_base_dir / "schema_mini.ttl"
        runner = CliRunner()
        result = runner.invoke(cli.cli, ["-i", schema_ttl, "-t", "sphn:Age"])
        assert result.exit_code == 0


class TestDataGeneration:
    def test_no_input_file(self):
        runner = CliRunner()
        result = runner.invoke(cli.cli)
        assert result.exit_code == 0

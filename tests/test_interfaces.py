import json
import shutil
import tempfile
from pathlib import Path

from rdflib import Graph

from sphn_mocker.interfaces import check_java_installed, generate_mapping, write_mock_data_to_ttl


def test_generate_mapping():
    test_data_base_dir = Path(__file__).parent / "test_data"
    schema_ttl = test_data_base_dir / "schema_mini.ttl"
    schema_simplified_ttl = test_data_base_dir / "schema_mini_simplified.ttl"

    json_schema, rml_mapping_file = generate_mapping(schema_ttl)

    assert rml_mapping_file.is_file()

    assert list(json_schema.keys()) == [
        "$schema",
        "type",
        "properties",
        "required",
        "additionalProperties",
    ]

    assert json_schema["properties"]["sphn:SubjectPseudoIdentifier"]["type"] == "object"

    assert not schema_simplified_ttl.is_file()


def test_write_mock_data_to_ttl():
    test_data_base_dir = Path(__file__).parent / "test_data"
    mock_data_json_file = test_data_base_dir / "schema_mini_mock_data.json"
    mapping_file_in = test_data_base_dir / "schema_mini_rml_generated_mapping.ttl"

    temp_dir = Path(tempfile.gettempdir())
    out_file = temp_dir / "mock_data.ttl"
    mapping_file = temp_dir / "schema_mini_rml_generated_mapping.ttl"
    shutil.copy(mapping_file_in, mapping_file)

    with mock_data_json_file.open(encoding="UTF-8") as fi:
        mock_data_json = json.load(fi)

    write_mock_data_to_ttl(mock_data_json=mock_data_json, mapping_file=mapping_file, output_file=out_file)

    try:
        g = Graph()
        g.parse(out_file)
        assert len(g) > 0
    finally:
        out_file.unlink()
        mapping_file.unlink()


def test_check_java_installed():
    check_java_installed()
